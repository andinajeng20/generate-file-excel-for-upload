<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Clear Filter</name>
   <tag></tag>
   <elementGuidId>a113a2f8-5deb-40c0-bad4-da68f8ee74e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#TbFilter-trigger-_trigger1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='TbFilter-trigger-_trigger1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>90c156e7-7d02-43dd-9c3f-10a0106b612e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>TbFilter-trigger-_trigger1</value>
      <webElementGuid>b9dd2e7a-3887-4cd6-b1f3-17355276cda8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-form-trigger x-form-trigger-default x-form-clear-trigger x-form-clear-trigger-default  x-form-trigger-over</value>
      <webElementGuid>24ef55c0-9338-4093-acd8-0d09f77e3cbc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>presentation</value>
      <webElementGuid>53e2b1d2-9320-46bf-9066-c7d2828d3d1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;TbFilter-trigger-_trigger1&quot;)</value>
      <webElementGuid>2ae8af39-9f7c-4fc2-bf22-e677e26e421c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Text File Generation/iframe</value>
      <webElementGuid>a07c82c5-8333-4615-99ac-c7745eefc48e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='TbFilter-trigger-_trigger1']</value>
      <webElementGuid>cee764a3-d58e-4c2d-8eb9-fadfcf78da3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='TbFilter-triggerWrap']/div[2]</value>
      <webElementGuid>ddeeaa5d-5e80-4dba-8b80-0d2468473839</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Filter:'])[1]/following::div[6]</value>
      <webElementGuid>a92c0e7f-6bd0-43db-8ff1-06b987a11420</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select Form(s) to Generate :'])[1]/following::div[12]</value>
      <webElementGuid>5193e568-7716-4d06-a66c-bfd762e2f38e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select All'])[1]/preceding::div[5]</value>
      <webElementGuid>76802d8f-fd64-422b-9ed1-cecfc0d8b41a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Expand All'])[1]/preceding::div[5]</value>
      <webElementGuid>b336b4da-da03-4252-aeab-a8cd89afbeba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/div/div[3]/div/div/div[2]</value>
      <webElementGuid>2f61cf00-a8fd-4535-9d9c-c373d5ac08fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'TbFilter-trigger-_trigger1']</value>
      <webElementGuid>c9a464cc-cbba-4ced-a40f-bbb9b041663b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
