<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Filter</name>
   <tag></tag>
   <elementGuidId>a1de5984-9b05-4bb6-9a52-5ef25b4e6e7d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#TbFilter-inputEl</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='TbFilter-inputEl']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>3d3a94af-96bb-4cca-bdd6-54c2a24efc9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>TbFilter-inputEl</value>
      <webElementGuid>dd8e602e-dd56-4a38-bf40-779484b52c36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>inputEl</value>
      <webElementGuid>7ec285fc-865d-48a6-9046-97b50338cd11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>d4a08e47-1885-44a4-90fe-cd60484cc5f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>size</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>3f8f8f01-2cfb-4d76-a4d7-f2feb927eeae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>TbFilter</value>
      <webElementGuid>5225971a-2901-4919-af23-82ecebd637b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>fdc06f1d-7243-422d-9abd-04ff87b07a23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>3240d9e6-1ec9-4387-bc6f-de2ad852cf79</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>textbox</value>
      <webElementGuid>79411c12-de5e-4c58-b050-fb295ee7779c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>aa309eeb-c509-4162-a9fe-1375e612305a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-readonly</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>6d7ad329-5ed6-4f9f-8f9a-11c055774c27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>TbFilter-ariaStatusEl</value>
      <webElementGuid>ec2b460a-18a6-4f2a-9512-513282025064</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>ab3a9a13-03c5-4906-940a-7320a7152c4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-form-field x-form-text x-form-text-default  x-form-empty-field x-form-empty-field-default x-form-focus x-field-form-focus x-field-default-form-focus</value>
      <webElementGuid>c9e0fe37-dbc3-4868-850b-aaffe41ab394</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>ee8f83af-a8ae-4799-b8e2-c213f3a85ab7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-componentid</name>
      <type>Main</type>
      <value>TbFilter</value>
      <webElementGuid>a5931f37-a52c-471e-a342-db937f6f3cfd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;TbFilter-inputEl&quot;)</value>
      <webElementGuid>ceb937ed-704b-4244-9f3b-2f38e594ceb5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Text File Generation/iframe</value>
      <webElementGuid>1398a3e4-d81f-44db-86db-8e81ba742687</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='TbFilter-inputEl']</value>
      <webElementGuid>932248fc-5c05-4cd4-9092-c624ee441344</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='TbFilter-inputWrap']/input</value>
      <webElementGuid>97ae5b60-937f-410a-a4b0-7a662e96dbfb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Filter:'])[1]/following::input[1]</value>
      <webElementGuid>7f39d9f9-7e88-4791-9fa4-dcd350b1e677</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select Form(s) to Generate :'])[1]/following::input[1]</value>
      <webElementGuid>95a4870b-f3eb-421f-982d-873a48ffbf5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select All'])[1]/preceding::input[1]</value>
      <webElementGuid>ade57719-702e-444f-9c88-295d65181781</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Expand All'])[1]/preceding::input[1]</value>
      <webElementGuid>bcb53a97-4f3c-4307-891f-8533e4a6de48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/div/div[3]/div/div/div/input</value>
      <webElementGuid>dee629ea-9281-4744-8cfb-c3f585f68da9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'TbFilter-inputEl' and @type = 'text' and @name = 'TbFilter']</value>
      <webElementGuid>306caad2-c9b6-47de-afbe-ae4c52c22ae7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
