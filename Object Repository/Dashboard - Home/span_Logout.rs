<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Logout</name>
   <tag></tag>
   <elementGuidId>2d427ac3-c425-42e6-9d39-25ab38efcbe6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='btnLogout-btnInnerEl']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#btnLogout-btnInnerEl</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>664f156b-68cc-4e4a-a9a6-26049889ad36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>btnLogout-btnInnerEl</value>
      <webElementGuid>00171b5c-bd9e-41c1-a131-fe5a0cf4ceff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>btnInnerEl</value>
      <webElementGuid>e78de734-e667-4b6b-bd9d-65c867ad9763</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>unselectable</name>
      <type>Main</type>
      <value>on</value>
      <webElementGuid>0e9610df-be62-49ec-8f74-225825295cdf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-btn-inner x-btn-inner-default-toolbar-small</value>
      <webElementGuid>16f9fc89-9035-4ae1-a33e-75cf036d630e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Logout</value>
      <webElementGuid>9d6a2806-b2c2-4c0b-91b4-d149b9a11c5a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;btnLogout-btnInnerEl&quot;)</value>
      <webElementGuid>88bed0b9-82e2-48a2-8c16-9aa0465c801c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='btnLogout-btnInnerEl']</value>
      <webElementGuid>3f21fd58-b4a9-4cc4-a042-669e31e9bac1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='btnLogout-btnEl']/span[2]</value>
      <webElementGuid>9b03aa33-aa4f-4ff7-b4df-0583996c6076</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Password'])[1]/following::span[4]</value>
      <webElementGuid>49843129-5d83-4431-8ac9-e477ace6b0fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Setting Branch &amp; Period'])[1]/following::span[8]</value>
      <webElementGuid>e7659af4-6aae-43d5-9b10-4808e23d9aeb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Filter:'])[1]/preceding::span[9]</value>
      <webElementGuid>fbd939b5-d86a-4a63-bc58-9822f1b21679</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cell value has been edited'])[1]/preceding::span[16]</value>
      <webElementGuid>af9c5a35-4092-44f6-9111-6ef222f07127</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[3]/span/span/span[2]</value>
      <webElementGuid>a4a4283f-33a7-4865-8308-9b8eec9b8b18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'btnLogout-btnInnerEl' and (text() = 'Logout' or . = 'Logout')]</value>
      <webElementGuid>caeecb14-f90f-4f9d-99ff-6be5f00e1aba</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
