<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Clear Kantor Cabang</name>
   <tag></tag>
   <elementGuidId>8df156e3-2889-4910-be6a-725b76a2d8c1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='cboKantorCabang-trigger-_trigger1']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#cboKantorCabang-trigger-_trigger1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>fe6e65d3-facc-4f21-80d9-96151837a50e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>cboKantorCabang-trigger-_trigger1</value>
      <webElementGuid>09e7d968-54eb-463e-acca-26fb8629766f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-form-trigger x-form-trigger-default x-form-clear-trigger x-form-clear-trigger-default  x-form-trigger-over</value>
      <webElementGuid>1707a6c5-f5c2-4f3a-98aa-7dbd209cc457</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>presentation</value>
      <webElementGuid>14535887-a499-4c74-86ed-28fa72e5ad29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cboKantorCabang-trigger-_trigger1&quot;)</value>
      <webElementGuid>d4bf1295-d650-4d7a-b78c-1e40619064da</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='cboKantorCabang-trigger-_trigger1']</value>
      <webElementGuid>9440388b-595b-446e-b1e6-877e0729457a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='cboKantorCabang-triggerWrap']/div[2]</value>
      <webElementGuid>1ee121af-46ff-406d-8ffc-d83778f07ee7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[1]/following::div[4]</value>
      <webElementGuid>1cddfff6-9bc5-442e-85e5-8c84f945f5ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Branch'])[1]/following::div[5]</value>
      <webElementGuid>7fe8581c-f867-4b65-80a3-b1a34f1aa8a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Internal Branch'])[1]/preceding::div[6]</value>
      <webElementGuid>5c0d00c6-4f5b-4326-acec-34b458a51903</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[2]/preceding::div[6]</value>
      <webElementGuid>2ab88352-b50f-4dfa-a5df-bf3c06b54886</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[11]/div[2]/div/div/div/div/div/div/div/div/div/div/div/div[2]</value>
      <webElementGuid>033e16f1-db85-4428-bf8e-eb73c051b99b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'cboKantorCabang-trigger-_trigger1']</value>
      <webElementGuid>bed2c400-0d37-4fe2-8410-580b69695f03</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
