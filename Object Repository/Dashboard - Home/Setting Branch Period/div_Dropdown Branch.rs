<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Dropdown Branch</name>
   <tag></tag>
   <elementGuidId>c0ea4364-f553-4020-9c86-15b33391bfba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='cboKantorCabang-trigger-picker']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#cboKantorCabang-trigger-picker</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3077ba6f-c3a6-4298-a15a-794283a8a6e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>cboKantorCabang-trigger-picker</value>
      <webElementGuid>f7bab1fe-aada-418a-b5d6-90678c6db296</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-form-trigger x-form-trigger-default x-form-arrow-trigger x-form-arrow-trigger-default  x-form-trigger-over</value>
      <webElementGuid>588f10ab-d1f8-4846-adf9-10d6d2d4e5f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>presentation</value>
      <webElementGuid>9cca2a09-2662-477e-9602-be37b02a3895</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cboKantorCabang-trigger-picker&quot;)</value>
      <webElementGuid>9b998220-fa74-4327-8cb7-1c329b80289d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='cboKantorCabang-trigger-picker']</value>
      <webElementGuid>7b330db0-2840-442a-82fa-28c37bf14890</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='cboKantorCabang-triggerWrap']/div[3]</value>
      <webElementGuid>6d1f1bab-a429-4c55-becc-dd8987c96ca3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[1]/following::div[5]</value>
      <webElementGuid>45ec8b4c-efbf-4d7c-80b5-e7bf7bd5a335</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Branch'])[1]/following::div[6]</value>
      <webElementGuid>776e3735-0e80-44d4-a28c-f5b64f78e14e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Internal Branch'])[1]/preceding::div[5]</value>
      <webElementGuid>2445e21a-327d-4372-a6cd-270fd817769c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[2]/preceding::div[5]</value>
      <webElementGuid>46e1a738-5fc1-46af-ac5b-c30d3cfda727</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[11]/div[2]/div/div/div/div/div/div/div/div/div/div/div/div[3]</value>
      <webElementGuid>1d7a8caf-3d83-4719-8671-7dceb2548afe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'cboKantorCabang-trigger-picker']</value>
      <webElementGuid>c82aad5f-8056-49c7-a329-c01759513571</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
