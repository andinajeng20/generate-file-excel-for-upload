<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_000000 - Gabungan KP</name>
   <tag></tag>
   <elementGuidId>ea5db5c1-7034-4971-8d7f-725dd2cfae31</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='cboKantorCabang-picker-listEl']/li[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'ext-element-31' and (text() = '000000 - Gabungan KP' or . = '000000 - Gabungan KP')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ext-element-31</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>42bed967-bc6d-4f3c-8f49-cf8aaddd30ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>option</value>
      <webElementGuid>33dab3bd-74bb-47d2-8956-8c9abe0a3fb5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>unselectable</name>
      <type>Main</type>
      <value>on</value>
      <webElementGuid>35133367-86d6-43e5-9c27-dce23d6ca0c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-boundlist-item x-boundlist-item-over</value>
      <webElementGuid>6d793bfc-04c4-4983-b64a-3d3499551217</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>c173845b-3667-4d08-90c2-0b614e3cde89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-recordindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>805ca7ba-b696-447f-adcf-856940c676b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-recordid</name>
      <type>Main</type>
      <value>117</value>
      <webElementGuid>bec43457-c98e-4e33-8d35-7fbde79bc6ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-boundview</name>
      <type>Main</type>
      <value>cboKantorCabang-picker</value>
      <webElementGuid>1e5decb8-8452-4e48-8e7b-a932e33eb1b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ext-element-31</value>
      <webElementGuid>d73da17c-4cc4-432a-b449-fd644667a249</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>000000 - Gabungan KP</value>
      <webElementGuid>6a2993f0-d5e5-4e71-b2c0-abc4e8270e5b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ext-element-31&quot;)</value>
      <webElementGuid>46b92111-e62d-4ef4-93b0-e9ef91b67eab</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='ext-element-31']</value>
      <webElementGuid>88f75de2-c1ce-429a-ae73-fb48dd626d53</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='cboKantorCabang-picker-listEl']/li</value>
      <webElementGuid>f8d436b4-02bc-4105-bd55-5e2c5eaa7e69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'ext-element-31', '&quot;', ')')])[1]/following::li[1]</value>
      <webElementGuid>d70fafab-c1cc-48bd-95b5-ccb96875aa58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::li[1]</value>
      <webElementGuid>ae9cded2-d29d-4aff-8dc1-a279424cb40d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Page'])[1]/preceding::li[1]</value>
      <webElementGuid>927e7d4a-458c-4f97-9ad6-106ba853c968</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='of 1'])[1]/preceding::li[1]</value>
      <webElementGuid>3cd1830d-192d-4d15-bf20-a3de96b070d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li</value>
      <webElementGuid>ee6a4084-7109-4a46-9b72-f5a594cc13bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = 'ext-element-31' and (text() = '000000 - Gabungan KP' or . = '000000 - Gabungan KP')]</value>
      <webElementGuid>02613bad-5534-47c2-87f4-92f369fc200e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
