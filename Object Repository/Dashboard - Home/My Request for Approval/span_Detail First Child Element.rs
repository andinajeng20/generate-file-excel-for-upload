<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Detail First Child Element</name>
   <tag></tag>
   <elementGuidId>f5d05cc4-8642-46fc-bccd-aefe18e6d0c0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Cell value has been edited'])[6]/following::span[10]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#button-1040-btnInnerEl</value>
      </entry>
      <entry>
         <key>IMAGE</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[@data-ref = 'btnInnerEl' and @class = 'x-btn-inner x-btn-inner-default-toolbar-small' and (text() = 'Detail' or . = 'Detail') and @ref_element = 'Object Repository/Dashboard - Home/My Request for Approval/iframe']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>6c04bcde-ca2b-456b-9786-b102b089e7af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>button-1040-btnInnerEl</value>
      <webElementGuid>9ff2afba-a9b0-47d6-9491-4ef10c344e8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>btnInnerEl</value>
      <webElementGuid>faabfd8b-ccc0-4bbb-9d30-88da2f7b9c67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>unselectable</name>
      <type>Main</type>
      <value>on</value>
      <webElementGuid>d1aca397-4c28-47ca-b6c5-4db3de422974</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-btn-inner x-btn-inner-default-toolbar-small</value>
      <webElementGuid>60023f85-b9d8-4836-8280-1a06397852af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Detail</value>
      <webElementGuid>130db048-1076-4498-8bc5-56b718c5a636</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;button-1040-btnInnerEl&quot;)</value>
      <webElementGuid>8051ab5e-ffd7-4711-9296-e3fe4d48e3a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Dashboard - Home/My Request for Approval/iframe</value>
      <webElementGuid>96fb816e-750e-4808-a9ff-bdbc95da9261</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='button-1040-btnInnerEl']</value>
      <webElementGuid>3f83542f-12fa-4cea-965f-eda8a87d7470</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='button-1040-btnEl']/span[2]</value>
      <webElementGuid>cce89159-ab9f-4701-89fd-c5d1d56facd7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RWA3 OP - FPKI Individu'])[1]/following::span[5]</value>
      <webElementGuid>4157074f-945e-425e-9054-766c807beece</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cell value has been edited'])[6]/following::span[10]</value>
      <webElementGuid>06ff9fd3-1812-461e-92f6-92928a1037c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RWA3 OP ATMR Untuk Risiko Operasional (D5) Individu'])[1]/preceding::span[2]</value>
      <webElementGuid>4bf11f8a-1b24-4722-a39d-f55511affd88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Detail'])[4]/preceding::span[4]</value>
      <webElementGuid>43ef316a-f854-42f4-9d68-d40f6d934bca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/span[2]</value>
      <webElementGuid>b94a06c5-c623-4576-a7c2-bfdfa5875615</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'button-1040-btnInnerEl' and (text() = 'Detail' or . = 'Detail')]</value>
      <webElementGuid>e6dc6f80-3e21-4c30-99b2-b1897b78a09e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
