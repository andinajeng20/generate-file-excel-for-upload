<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_User ID is required</name>
   <tag></tag>
   <elementGuidId>191b5941-7194-4b3e-b818-7978457784ab</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='RequiredFieldValidator1']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#RequiredFieldValidator1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>3b4f8ae5-b9ae-4900-99d6-c8846e15921f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>RequiredFieldValidator1</value>
      <webElementGuid>c6ef2a46-7894-4a82-9b3c-691fcac1171d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>help-block</value>
      <webElementGuid>fa096d55-a273-476f-af64-090ac2ff9983</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>User ID is required</value>
      <webElementGuid>08caeaed-aed9-48f4-8d87-503f4bb1572b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;RequiredFieldValidator1&quot;)</value>
      <webElementGuid>5ad63a9f-b8ad-4471-8de1-ddf801bb3dd0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='RequiredFieldValidator1']</value>
      <webElementGuid>7f890f8a-b386-4e9a-9b99-85452e57273d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form1']/div[3]/span</value>
      <webElementGuid>67022eb6-157f-408e-b30f-51aa5ab64fea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password is required'])[1]/preceding::span[1]</value>
      <webElementGuid>d0c4393a-cc26-47a5-86fa-c6227405e08e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign In'])[1]/preceding::span[4]</value>
      <webElementGuid>b401456f-8a05-4f79-a317-94f4d6c35197</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span</value>
      <webElementGuid>9fec57e5-9b25-4435-b10b-6c6960abf0bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'RequiredFieldValidator1' and (text() = 'User ID is required' or . = 'User ID is required')]</value>
      <webElementGuid>26621631-6efe-428b-8df0-df8cda01924e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
