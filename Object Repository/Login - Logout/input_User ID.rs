<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_User ID</name>
   <tag></tag>
   <elementGuidId>0ed97c96-5dad-4ace-8a41-4df2aee7104c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='txtUsername']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#txtUsername</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>0d0103db-4309-41a1-85b0-fe3dff962538</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>txtUsername</value>
      <webElementGuid>1e6da386-e11d-480f-8883-75481a7db3b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>18840c21-c8be-4267-98ed-959e00e65e57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>txtUsername</value>
      <webElementGuid>e7c5fb60-6071-4f4c-af31-a893484b1921</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>54440b93-a584-4608-b273-c03443f0d9fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>User ID</value>
      <webElementGuid>cd8964c0-ff4b-479f-9016-32c53503fe87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;txtUsername&quot;)</value>
      <webElementGuid>750e0a58-d8ee-49df-a6cd-adac02311e05</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='txtUsername']</value>
      <webElementGuid>a38814ec-9aef-4ea1-9a62-21f1c238c0c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form1']/div[3]/input</value>
      <webElementGuid>ec5817fe-7767-4c8f-b373-03ffcce8d540</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='User ID is required'])[1]/preceding::input[1]</value>
      <webElementGuid>9b08945a-1fb0-421f-9b08-e1a3224e8fa2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password is required'])[1]/preceding::input[2]</value>
      <webElementGuid>15e1bdaf-be97-4a07-a0b6-ef8ecb1e21bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>113131d8-b16f-44d4-818d-eb17f2de8271</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'txtUsername' and @type = 'text' and @id = 'txtUsername' and @placeholder = 'User ID']</value>
      <webElementGuid>cb5f1702-8c9f-4f20-863f-f102da11e93b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
