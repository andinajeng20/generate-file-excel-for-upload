<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__Jumlah_Transaksi</name>
   <tag></tag>
   <elementGuidId>4ea1c1c0-04fb-4b71-b19a-e4e3ef877091</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='Jumlah_Transaksi-inputEl']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#Jumlah_Transaksi-inputEl</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>b8919fc8-6c87-4713-87e1-43aedf128f21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Jumlah_Transaksi-inputEl</value>
      <webElementGuid>67cea264-5075-46bc-aac3-c50becd85a1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>inputEl</value>
      <webElementGuid>cbbc25a0-cb36-451b-95e4-3f4150fe5acd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>976a4bfa-7766-4e29-97fc-59700bf1d0fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>size</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>5d9c08df-acc9-45f4-aafc-47bfc7289dbb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Jumlah_Transaksi</value>
      <webElementGuid>cc640dfe-a5f5-4afb-b386-b17a0a32dd7c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>21a9e77c-7681-45bf-86f3-0eaddccd424d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>948e4521-45e3-40fa-a729-21e761e164bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>spinbutton</value>
      <webElementGuid>19b2f61f-50f0-467e-8939-225bea8219a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>53f40ff7-e587-442c-9a20-fce232edab17</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-readonly</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>c07095cf-5b4d-41ec-8666-8a25f2938641</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>Jumlah_Transaksi-ariaStatusEl</value>
      <webElementGuid>1897ed86-8815-4482-a1c7-47925e3191a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>d0c542ba-bca0-4d19-8b5d-09550746d9e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-valuemin</name>
      <type>Main</type>
      <value>-2147483648</value>
      <webElementGuid>ecae38c2-ea29-424d-accd-64a58110ded6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-valuemax</name>
      <type>Main</type>
      <value>2147483647</value>
      <webElementGuid>f8406f64-0e6e-492d-83a6-54abe7ad4a83</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-form-field x-form-required-field x-form-text x-form-text-default  x-form-empty-field x-form-empty-field-default x-form-invalid-field x-form-invalid-field-default x-form-focus x-field-form-focus x-field-default-form-focus</value>
      <webElementGuid>9ff62a75-ef3f-4825-9b5d-0b110bbe7827</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>0f6af59c-77b6-4d20-81da-948c7f6ff969</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-componentid</name>
      <type>Main</type>
      <value>Jumlah_Transaksi</value>
      <webElementGuid>16966240-40f8-4975-9887-6f514aacab2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Jumlah_Transaksi-inputEl&quot;)</value>
      <webElementGuid>02d23633-11a7-4061-a603-ed210d07f204</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/View - Upload/View/Add - Edit - Delete/iframe</value>
      <webElementGuid>2b160106-4ba6-40fa-9f14-523b42b8bf73</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='Jumlah_Transaksi-inputEl']</value>
      <webElementGuid>46a86ddb-02c9-43c0-83ba-29caf6d694f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Jumlah_Transaksi-inputWrap']/input</value>
      <webElementGuid>4ee0eda8-625c-4306-af8e-74abbe2928a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[9]/following::input[1]</value>
      <webElementGuid>033a965e-ab6c-4b8d-8cb3-72084202e9e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jumlah Transaksi'])[1]/following::input[1]</value>
      <webElementGuid>7a92a956-5e61-4617-bddd-38b6170b28c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jumlah Transaksi is required.'])[2]/preceding::input[1]</value>
      <webElementGuid>4658fe39-45ac-48c1-96ce-03ecb5273c6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Volume (Rp)'])[1]/preceding::input[1]</value>
      <webElementGuid>21aef61c-bf6a-4f70-8826-f9e12b3260a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[9]/div/div/div/input</value>
      <webElementGuid>722b2b3b-8dc7-4be8-bff1-79b761acc4cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'Jumlah_Transaksi-inputEl' and @type = 'text' and @name = 'Jumlah_Transaksi']</value>
      <webElementGuid>cf9fa87f-4eb1-4652-909d-511877883895</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
