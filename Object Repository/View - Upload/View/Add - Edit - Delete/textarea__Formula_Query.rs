<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea__Formula_Query</name>
   <tag></tag>
   <elementGuidId>e4bfc8d3-1a8d-408d-948a-74958a117930</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#Formula_Query-inputEl</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id='Formula_Query-inputEl']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>f84f3b3c-2615-493b-93fd-544e1b9c72de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Formula_Query-inputEl</value>
      <webElementGuid>98d44ac5-6f2b-4191-8f8f-393a7e52b642</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>inputEl</value>
      <webElementGuid>64e184b2-2aff-4f22-91f1-de6db599dbce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Formula_Query</value>
      <webElementGuid>a5545419-02e4-4093-9438-27a243b95798</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-form-field x-form-text x-form-text-default x-form-textarea x-form-empty-field x-form-empty-field-default x-form-focus x-field-form-focus x-field-default-form-focus</value>
      <webElementGuid>82dfe222-6748-40e1-b7be-db881fda505f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>6be98583-3907-4daf-b493-76a0c98b1436</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>26be0c1d-65c6-42ba-879a-403acd2e2273</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-multiline</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>12ad9b16-ab04-4e6c-977c-3627c19aa2df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>textbox</value>
      <webElementGuid>8078eb76-bcb5-4b90-9867-25333db4aa1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>fe59680c-c07f-4d37-8212-c8b3997c1d07</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-readonly</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>892ec073-11ea-452b-8535-56530c5f821c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>Formula_Query-ariaStatusEl</value>
      <webElementGuid>30e5af53-5e34-4672-abca-6b78a4627557</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>91c3e163-410e-4533-9c10-6725793fdc92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>237ec20a-6432-46a6-8767-5273e300f701</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-componentid</name>
      <type>Main</type>
      <value>Formula_Query</value>
      <webElementGuid>1f43b697-4689-4ac2-92b2-70bf5db4d20e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Formula_Query-inputEl&quot;)</value>
      <webElementGuid>d6d44124-7f77-410f-afc1-5ce9e782cd55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/View - Upload/View/Add - Edit - Delete/iframe</value>
      <webElementGuid>ee7286d0-493e-428a-aa59-a79b3f1dd8fa</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@id='Formula_Query-inputEl']</value>
      <webElementGuid>ec90f66e-4e1f-4c5e-8141-906b7e06c936</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Formula_Query-inputWrap']/textarea</value>
      <webElementGuid>f1e68f5f-e8f2-4a6d-a7ac-d0fc9375c3bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[5]/following::textarea[1]</value>
      <webElementGuid>28bb2214-b83b-43c4-b715-ec83b2dacee0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Formula Query'])[1]/following::textarea[1]</value>
      <webElementGuid>4d380c4a-9f09-4572-a480-f20fb520a2c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Group ID'])[1]/preceding::textarea[1]</value>
      <webElementGuid>cb00be01-626a-401d-80f0-77729f07a6e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[6]/preceding::textarea[1]</value>
      <webElementGuid>7cff11e4-d541-4397-86c1-443e9b8cf625</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/textarea</value>
      <webElementGuid>852bdb18-786f-40b5-b779-ed595dc5a311</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@id = 'Formula_Query-inputEl' and @name = 'Formula_Query']</value>
      <webElementGuid>4fd56cf8-ab4e-4ebe-b264-2ae29e69b76d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
