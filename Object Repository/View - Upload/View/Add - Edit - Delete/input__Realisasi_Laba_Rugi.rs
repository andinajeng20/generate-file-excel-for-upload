<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__Realisasi_Laba_Rugi</name>
   <tag></tag>
   <elementGuidId>c441d6cc-2301-4d6c-b99b-d45a51552d73</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='Realisasi_Laba_Rugi-inputEl']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#Realisasi_Laba_Rugi-inputEl</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>04689546-0265-4e0c-bb60-308bbe1762bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Realisasi_Laba_Rugi-inputEl</value>
      <webElementGuid>87c13032-7a11-4f5e-ad9c-7f72b6fd00f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>inputEl</value>
      <webElementGuid>67798d0f-80cd-4c10-bdcb-f470594b8911</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>9171e008-df81-45c8-ac80-aa337a92cae3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>size</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>e6f0befc-8f58-40c9-9aca-f8ca62acccc3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Realisasi_Laba_Rugi</value>
      <webElementGuid>326d0447-d4c6-4c0f-91bd-3636338dcbaa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>40315ed8-ee6a-4800-ad3f-2f26675c70a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>61ae8155-769b-4a38-91b6-d6053b73859c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>spinbutton</value>
      <webElementGuid>4d0615c1-4d0c-4eae-8d25-b1686e9ed5bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>14c4b004-0b80-411a-ad50-04597965b25e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-readonly</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>e9e593c3-5d96-42e0-b0dd-fdfeae321124</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>Realisasi_Laba_Rugi-ariaStatusEl</value>
      <webElementGuid>5f986891-3b2c-44b3-87b4-2df2146e402e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>daee3f3d-09d2-4eb1-9d3a-05f389a845dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-form-field x-form-required-field x-form-text x-form-text-default  x-form-empty-field x-form-empty-field-default x-form-invalid-field x-form-invalid-field-default x-form-focus x-field-form-focus x-field-default-form-focus</value>
      <webElementGuid>4594e369-0fb4-44a4-9193-bc09ee580b8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>94ec4aa8-1614-44c1-ac8c-89e4015d11cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-componentid</name>
      <type>Main</type>
      <value>Realisasi_Laba_Rugi</value>
      <webElementGuid>a206e05e-10cb-4773-a2bc-8a44abf9136a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Realisasi_Laba_Rugi-inputEl&quot;)</value>
      <webElementGuid>df0a5009-58a3-4f57-b5eb-5b633b0547e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/View - Upload/View/Add - Edit - Delete/iframe</value>
      <webElementGuid>e315a63f-60e9-4d28-afe5-08a80b9d1e6c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='Realisasi_Laba_Rugi-inputEl']</value>
      <webElementGuid>9c5806d8-dadb-4861-9104-3d9332416b07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Realisasi_Laba_Rugi-inputWrap']/input</value>
      <webElementGuid>a06b532a-77e3-4274-987e-2088b9b85e11</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[12]/following::input[1]</value>
      <webElementGuid>9a89f7f2-b579-4836-a3bd-28e237a8c410</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Realisasi Laba Rugi'])[1]/following::input[1]</value>
      <webElementGuid>fa013504-a636-40aa-b29e-43c65c97db05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Realisasi Laba Rugi is required.'])[2]/preceding::input[1]</value>
      <webElementGuid>46b5a03a-9f07-41bf-8c58-c3496331cc3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama Pembeli'])[1]/preceding::input[1]</value>
      <webElementGuid>b159eb21-d90b-41c9-b40a-c8225a28b466</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[12]/div/div/div/input</value>
      <webElementGuid>90e11ec2-ddf8-4baf-b216-3708dcae79f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'Realisasi_Laba_Rugi-inputEl' and @type = 'text' and @name = 'Realisasi_Laba_Rugi']</value>
      <webElementGuid>9fe4f755-7280-4638-87c0-1fb7e12a9e9c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
