<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__FK_Jenis_Penggunaan</name>
   <tag></tag>
   <elementGuidId>45d20fc1-2469-4a40-bf7c-ff4d5c731d41</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#FK_Jenis_Penggunaan-inputEl</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='FK_Jenis_Penggunaan-inputEl']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>c29ad6b5-b093-48a1-b389-ef48a7a39ee8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>FK_Jenis_Penggunaan-inputEl</value>
      <webElementGuid>666c194d-b680-4ac0-a085-e86d0be1369a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>inputEl</value>
      <webElementGuid>6234f230-f9b5-4c84-a1d8-f9572dc146f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>8365025c-45f7-47df-916e-33db1b4f6662</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>size</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>db0fea5a-3bcf-48dd-a2ad-09178af9d9e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>FK_Jenis_Penggunaan</value>
      <webElementGuid>669f2686-cb0c-438f-9354-4252ecbeb3f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>ce2b82b4-8a2a-4c05-ad7c-fd4e513ffee0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>e05f8ee5-2c0a-44e9-abd2-b4e448e40799</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>combobox</value>
      <webElementGuid>1e2045fc-835b-4804-846e-da725f098af3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>a04df777-3d27-45e6-bcbe-1926b87f2c29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>ce427bb5-a8c1-4e78-9250-8169c8573ca7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-owns</name>
      <type>Main</type>
      <value>FK_Jenis_Penggunaan-inputEl FK_Jenis_Penggunaan-picker-listEl</value>
      <webElementGuid>c4fd0e15-0140-4efb-a952-450d35280dcd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-autocomplete</name>
      <type>Main</type>
      <value>list</value>
      <webElementGuid>7d941157-7ce1-4e8c-be57-468c981f5aee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>beb10376-03b9-4e86-9f7d-dc01c862de1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-readonly</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>d45d93ac-0dcb-42e1-ba3c-c41670c52d37</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>FK_Jenis_Penggunaan-ariaStatusEl</value>
      <webElementGuid>2fcf95c5-5813-4d6d-9a3e-1f583ad0f948</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>40e1b7d6-881a-4f86-a29f-8fe50fef50e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-form-field x-form-required-field x-form-text x-form-text-default  x-form-focus x-field-form-focus x-field-default-form-focus</value>
      <webElementGuid>5184baff-1ce6-4464-affa-7beb036d0ece</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>5139e1b4-6759-495b-b7cc-9c7d9b6628d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-componentid</name>
      <type>Main</type>
      <value>FK_Jenis_Penggunaan</value>
      <webElementGuid>3bfc7d9d-fb69-4eed-b4f7-8546e4bd39af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FK_Jenis_Penggunaan-inputEl&quot;)</value>
      <webElementGuid>0b5fc448-e828-4574-b915-6c3fff4c4e3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/View - Upload/View/Add - Edit - Delete/iframe</value>
      <webElementGuid>59f7c49f-7e4d-4c6e-a33e-8bcde43d3174</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='FK_Jenis_Penggunaan-inputEl']</value>
      <webElementGuid>d7d3551d-ebcd-4091-9c52-ba2091ff7e0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FK_Jenis_Penggunaan-inputWrap']/input</value>
      <webElementGuid>b1584524-59bd-4449-b298-3ee011386957</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[12]/following::input[1]</value>
      <webElementGuid>6f753ea1-f2ec-4cf2-93b8-ea386e7fda4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jenis Penggunaan'])[1]/following::input[1]</value>
      <webElementGuid>56247fb4-9a08-4d8e-a70c-6d5bfe62feed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tanggal Mulai'])[1]/preceding::input[2]</value>
      <webElementGuid>6fdb17f9-436c-4a6b-9fa9-d6681ab1049a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[13]/preceding::input[2]</value>
      <webElementGuid>9561f226-03fd-45b1-9ba0-d695b567b5ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[12]/div/div/div/input</value>
      <webElementGuid>9cf12d96-8d78-4b89-834a-a2ae4dcfba92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'FK_Jenis_Penggunaan-inputEl' and @type = 'text' and @name = 'FK_Jenis_Penggunaan']</value>
      <webElementGuid>8b3f0347-6d29-4e68-99eb-3b588dc63804</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
