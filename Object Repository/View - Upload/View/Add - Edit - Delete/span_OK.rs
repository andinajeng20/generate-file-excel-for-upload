<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_OK</name>
   <tag></tag>
   <elementGuidId>fe4913b0-cd61-4cfd-884c-3d6e2b8b7cfb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder1_BtnConfirmation-btnEl</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='ContentPlaceHolder1_BtnConfirmation-btnWrap']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ee1ff801-cc42-403a-b1b2-68a884644097</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder1_BtnConfirmation-btnEl</value>
      <webElementGuid>11128b3a-ce8d-4dfd-9030-29a2b1831e1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>btnEl</value>
      <webElementGuid>0c780b40-079e-4c59-81b1-964c943a55a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>presentation</value>
      <webElementGuid>75f29ad6-a905-4b89-840d-2e0c8fbbb1a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>unselectable</name>
      <type>Main</type>
      <value>on</value>
      <webElementGuid>2a4c6e3e-3bba-4919-95d9-a4de43ebee28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-btn-button x-btn-button-default-small x-btn-text  x-btn-icon x-btn-icon-left x-btn-button-center </value>
      <webElementGuid>461b7772-1307-4bfe-b295-d2c4bd6bc904</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>OK</value>
      <webElementGuid>bc6924ae-b322-4189-9b13-0eeb391e9fd5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder1_BtnConfirmation-btnEl&quot;)</value>
      <webElementGuid>1e2e3828-6f84-4488-9e84-1403ab406e71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/View - Upload/View/Add - Edit - Delete/iframe</value>
      <webElementGuid>e80b685f-b8af-447f-bd30-792e7cf59e13</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='ContentPlaceHolder1_BtnConfirmation-btnEl']</value>
      <webElementGuid>568287db-c5c6-4583-b6ca-1fe00a1b6cae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='ContentPlaceHolder1_BtnConfirmation-btnWrap']/span</value>
      <webElementGuid>af277eff-5a57-4318-8f9f-e1473eca8394</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Saved into Pending Approval'])[1]/following::span[3]</value>
      <webElementGuid>8b9b128d-f89a-4d58-95db-bb3f47e372d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirmation'])[1]/following::span[5]</value>
      <webElementGuid>815621be-5eed-42b0-ae15-29a7576c4196</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RWA3 OP - Datasource Kerugian Operasional Add'])[1]/preceding::span[5]</value>
      <webElementGuid>7faf608d-92aa-40c0-9775-27c2d9003a68</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/span</value>
      <webElementGuid>2c695300-628a-4f1b-bd97-ea51c10ff55d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'ContentPlaceHolder1_BtnConfirmation-btnEl' and (text() = 'OK' or . = 'OK')]</value>
      <webElementGuid>b96fc29b-50cb-44f7-8c08-b7926e78fb1d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
