<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__File_PendukungName</name>
   <tag></tag>
   <elementGuidId>49a872df-e910-4952-8a77-e80608142dd8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolder1_File_PendukungName-inputEl']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder1_File_PendukungName-inputEl</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>b30910bc-dde5-41c2-9ccd-170a3daf78d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder1_File_PendukungName-inputEl</value>
      <webElementGuid>79468a7a-c813-4434-be0d-b54c3f87ffe8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>inputEl</value>
      <webElementGuid>d4107208-c186-4a52-addc-8599c1fa3182</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>8655e08b-55ec-4bd3-a6b4-4a9eeed0da20</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>size</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>fa588bda-d5e8-4f41-a93b-9557dc536d1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ContentPlaceHolder1_File_PendukungName</value>
      <webElementGuid>fd2e88f3-344a-4a2a-b14f-7d650a807dbf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>readonly</name>
      <type>Main</type>
      <value>readonly</value>
      <webElementGuid>cf38d6e7-18b3-42b3-bc89-3ed2c7a80a1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>ccfb9b45-179e-4d29-948d-a360b59ce554</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>83c6bc95-d2fc-4990-bbf7-c9b4acf747f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>textbox</value>
      <webElementGuid>1a91a31c-5dbf-4929-a3b5-185cd29f723b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>a04ee3eb-07df-41a4-8eca-a16feca06477</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-readonly</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>7f1d5f2f-8f1e-420f-9fc5-3946931f2a29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>ContentPlaceHolder1_File_PendukungName-ariaStatusEl</value>
      <webElementGuid>930545de-42fa-4281-a653-ef4f7578d1c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>2108641c-6c49-4aca-94c7-da501e23e588</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-form-field x-form-required-field x-form-text x-form-text-default  x-form-focus x-field-form-focus x-field-default-form-focus</value>
      <webElementGuid>b1cb0c23-3cda-4425-a772-5bf412b347a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>eab5b794-025c-433f-af53-e0f5667663fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-componentid</name>
      <type>Main</type>
      <value>ContentPlaceHolder1_File_PendukungName</value>
      <webElementGuid>71e2dd14-b055-4797-ba2f-23b63d47261d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder1_File_PendukungName-inputEl&quot;)</value>
      <webElementGuid>526eb943-d983-47f8-ac16-0b542b379388</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/View - Upload/View/Add - Edit - Delete/iframe</value>
      <webElementGuid>f31230be-b485-4890-b94b-e5f5bb506eec</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolder1_File_PendukungName-inputEl']</value>
      <webElementGuid>e585834a-f55d-4403-a308-1480a4f5bcf1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder1_File_PendukungName-inputWrap']/input</value>
      <webElementGuid>b7c4168e-b10e-4211-8b5d-9d106639c51c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[7]/following::input[1]</value>
      <webElementGuid>99289a94-74e7-4e9e-979d-4070b231f33f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='File_Pendukung Name'])[1]/following::input[1]</value>
      <webElementGuid>475f8f23-f08f-4272-9ecc-39c495743ddb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mark As Delete'])[1]/preceding::input[2]</value>
      <webElementGuid>a2ebc608-3ff3-4253-b24b-a1bbe81cf1ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[8]/preceding::input[2]</value>
      <webElementGuid>8a3fa5ee-adf1-4fcf-bdca-6880ad0dcb2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div/input</value>
      <webElementGuid>25b1788b-438d-4f3f-aa8e-5a5c0f0c1788</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'ContentPlaceHolder1_File_PendukungName-inputEl' and @type = 'text' and @name = 'ContentPlaceHolder1_File_PendukungName']</value>
      <webElementGuid>5e0ba6c5-06d3-45b0-a91c-f8d2b7e19897</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
