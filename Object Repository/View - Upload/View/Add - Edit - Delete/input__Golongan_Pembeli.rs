<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__Golongan_Pembeli</name>
   <tag></tag>
   <elementGuidId>8b81081c-3548-418f-b00d-8590765f8c3d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='Golongan_Pembeli-inputEl']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#Golongan_Pembeli-inputEl</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>2e28d0e7-c341-4100-8b9f-e04d54ac65ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Golongan_Pembeli-inputEl</value>
      <webElementGuid>aabc4c7b-0d6a-4037-85a2-7c8ff049d278</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>inputEl</value>
      <webElementGuid>a7c8c8fe-9b94-4d29-b52c-8cbb941314a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>02bae4de-707a-4f74-be8a-5277a3a67140</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>size</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>9f6f2fbf-5e0b-4c9e-82e1-829a57a07fd4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Golongan_Pembeli</value>
      <webElementGuid>fd1c9e67-6d73-4099-8e8a-6b635d08a773</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>ceab8a2d-94ad-43db-949e-5aeb8f59cb4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>89f96eb0-e7bf-4b6e-bbfc-22a0effa69e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>combobox</value>
      <webElementGuid>35c930fc-53ad-43ee-9f87-27ab19dd5f23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>90c5d67e-64fe-4a39-be2a-9faf739279ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>422c396b-5c6f-46cb-b46e-c7a3153fe305</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-owns</name>
      <type>Main</type>
      <value>Golongan_Pembeli-inputEl Golongan_Pembeli-picker-listEl</value>
      <webElementGuid>c9cd9b79-76ce-4a2b-bc06-13f7ac80fe9d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-autocomplete</name>
      <type>Main</type>
      <value>list</value>
      <webElementGuid>58d5cafb-dc4a-4e49-9c11-e8b5e4585885</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>8000c71a-033a-4f38-bbe8-c3a0f493fd3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-readonly</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>d284b309-3663-4dfb-a91c-7d5d8a2757db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>Golongan_Pembeli-ariaStatusEl</value>
      <webElementGuid>4ea882f6-2124-488b-9619-5a7ce63f062e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>c0e623f0-0b8c-47d2-bb01-350641132083</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-form-field x-form-required-field x-form-text x-form-text-default  x-form-focus x-field-form-focus x-field-default-form-focus</value>
      <webElementGuid>313ccc48-7396-4520-8fda-f8696921fb12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>d9941d73-d173-49f2-ae0d-f7001f9ba4f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-componentid</name>
      <type>Main</type>
      <value>Golongan_Pembeli</value>
      <webElementGuid>21e0eba2-4dc2-4167-a9f7-52f42118d283</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Golongan_Pembeli-inputEl&quot;)</value>
      <webElementGuid>afe66b7a-f3a4-46b6-9464-8ac499342029</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/View - Upload/View/Add - Edit - Delete/iframe</value>
      <webElementGuid>b4d917c0-8f23-427d-9170-deaa9d9cb8c3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='Golongan_Pembeli-inputEl']</value>
      <webElementGuid>b0fe5a32-b96f-468e-9c8e-77de5f15a4c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Golongan_Pembeli-inputWrap']/input</value>
      <webElementGuid>7af50eb8-dedf-4194-a052-a466e5870494</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[14]/following::input[1]</value>
      <webElementGuid>e1641c5e-4159-45e7-ac5d-6c0b6a9a6542</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Golongan Pembeli'])[1]/following::input[1]</value>
      <webElementGuid>55d395ab-7905-4232-8ce8-8846983e4b1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mata Uang'])[1]/preceding::input[2]</value>
      <webElementGuid>76a7a177-40e6-4072-811d-5bc746afc814</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[15]/preceding::input[2]</value>
      <webElementGuid>e98f1bfa-78f2-479a-88b9-48da736e70ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[14]/div/div/div/input</value>
      <webElementGuid>673c6a19-f6bc-472e-baf0-9360968c7040</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'Golongan_Pembeli-inputEl' and @type = 'text' and @name = 'Golongan_Pembeli']</value>
      <webElementGuid>a0f4c102-2421-4b18-ad9f-c5d442095803</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
