<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__Sandi_Source</name>
   <tag></tag>
   <elementGuidId>755f4b5b-24ba-43e6-9118-286527281d50</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#Sandi_Source-inputEl</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='Sandi_Source-inputEl']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>8e36ca61-5cac-4ac5-a7b7-f986bbd9af0e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Sandi_Source-inputEl</value>
      <webElementGuid>0404b4ab-5fba-41d8-9f5b-ca74c844d515</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>inputEl</value>
      <webElementGuid>c44e16a8-55d5-46cd-a470-6dda29962698</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>765fc982-db16-4d7d-ae76-419c9ea942fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>size</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>d972768a-81c1-4332-a2e5-1c38ab0f0566</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Sandi_Source</value>
      <webElementGuid>2c0e703a-7982-4d1f-a983-4b9567f291fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>30</value>
      <webElementGuid>ba183897-bc50-42b3-ae60-bdeb5f50cc71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>43dd0838-a31a-4d36-bacc-69747baacd2c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>ee87b0f4-28cb-4cc5-b9ce-db2678c7935d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>textbox</value>
      <webElementGuid>9f7e9279-3e6f-454f-b8ca-f129dabcf27c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>7e21e777-bd83-4641-a5fa-7071c37e59d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-readonly</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>407f05c0-8754-44e4-b3dc-5dfaf252cc0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>Sandi_Source-ariaStatusEl</value>
      <webElementGuid>65027801-7f59-4693-b394-12a36761a811</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>5f0919f6-6126-4847-99bb-5a110f547902</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-form-field x-form-required-field x-form-text x-form-text-default  x-form-focus x-field-form-focus x-field-default-form-focus</value>
      <webElementGuid>6a826d30-f789-4823-a33a-9b047c231fd3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>ca06c5b8-93e1-49c4-a368-71d688f0dbb9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-componentid</name>
      <type>Main</type>
      <value>Sandi_Source</value>
      <webElementGuid>ef90ad09-17d5-4441-a5ac-cdeceebd5628</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Sandi_Source-inputEl&quot;)</value>
      <webElementGuid>1a8023b0-8f0f-4165-b2e1-ff38a067f088</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/View - Upload/View/Add - Edit - Delete/iframe</value>
      <webElementGuid>58855d65-cec6-498b-8968-0c8d76d5957a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='Sandi_Source-inputEl']</value>
      <webElementGuid>dbdfc4ff-830f-4986-83e4-3447250eb4f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Sandi_Source-inputWrap']/input</value>
      <webElementGuid>ea4659ba-2d14-411f-907f-376a4640cf98</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[4]/following::input[1]</value>
      <webElementGuid>991b9cbd-9224-44a7-aa37-6b190eae3e62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sandi Source'])[1]/following::input[1]</value>
      <webElementGuid>6dbedbbe-1018-450d-9017-0f08e66f908d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Faktor Pengali'])[1]/preceding::input[1]</value>
      <webElementGuid>334159d2-9d76-4f3d-b392-40b77a9ceff6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[5]/preceding::input[1]</value>
      <webElementGuid>cc662308-6829-4340-8e5d-06ea528f1879</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/input</value>
      <webElementGuid>28eaf38d-9cd9-414a-bb1d-3dd174ab38f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'Sandi_Source-inputEl' and @type = 'text' and @name = 'Sandi_Source']</value>
      <webElementGuid>f3b86b11-470c-4ab7-833a-7281b50e96b2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
