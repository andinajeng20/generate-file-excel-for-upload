<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__Jumlah_BiayaPerolehan</name>
   <tag></tag>
   <elementGuidId>36e073a8-712f-410a-ab5f-ce22cb33c952</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='Jumlah_BiayaPerolehan-inputEl']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#Jumlah_BiayaPerolehan-inputEl</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>8aa53cb6-b220-460e-aa8d-98bd2a797ff5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Jumlah_BiayaPerolehan-inputEl</value>
      <webElementGuid>c4ba9f6e-4672-4e93-a519-77c65e1bc4b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>inputEl</value>
      <webElementGuid>a4c0f826-e78e-4d06-8f33-b272fcc7f70e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>d47c9865-16ff-41d6-bb69-ccebad723334</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>size</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>d34fe67d-4131-4dee-b9d2-defe1b37149b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Jumlah_BiayaPerolehan</value>
      <webElementGuid>649ed0a9-bcb0-4b4f-95bb-452740fcb4ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>71e5ffe3-4707-49bc-bb6b-093d6e4ccf7f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>59dadc7b-d5ed-41b3-9884-aef10fbe4b4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>spinbutton</value>
      <webElementGuid>b0e5cf3b-7afe-483f-8b13-fd199bd7b7dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>3c656827-a72b-4d48-b690-a973f0249388</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-readonly</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>5241d5e3-2ca1-48b4-99c9-820273aa27b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>Jumlah_BiayaPerolehan-ariaStatusEl</value>
      <webElementGuid>95ba55df-67ea-4ef4-b09f-3aedc042f539</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>548118b7-aab4-4e9e-aa4f-567ee61fec64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-form-field x-form-required-field x-form-text x-form-text-default  x-form-focus x-field-form-focus x-field-default-form-focus</value>
      <webElementGuid>bcdc37e1-0d7b-4054-a3ad-acf1719c0026</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>38812316-ed4b-43fa-b8e0-840c1665ddce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-componentid</name>
      <type>Main</type>
      <value>Jumlah_BiayaPerolehan</value>
      <webElementGuid>a147ab4e-9ea5-4d61-86d7-dda8e46e5456</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Jumlah_BiayaPerolehan-inputEl&quot;)</value>
      <webElementGuid>025afcf2-df7e-4f41-a407-39123e2709ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/View - Upload/View/Add - Edit - Delete/iframe</value>
      <webElementGuid>6d5c5d27-a5ed-4220-b9af-d5d3a3aa56c3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='Jumlah_BiayaPerolehan-inputEl']</value>
      <webElementGuid>e9626005-0bff-4e52-9bc2-1a496c6e9390</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Jumlah_BiayaPerolehan-inputWrap']/input</value>
      <webElementGuid>4c685ccf-4879-4cda-b07d-da228567c487</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[10]/following::input[1]</value>
      <webElementGuid>fef00686-1fb5-490f-92c0-66e5618a0160</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jumlah(BiayaPerolehan)'])[1]/following::input[1]</value>
      <webElementGuid>2f559d88-ac6d-40ef-be53-ccf371bda444</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Golongan Pihak Lawan'])[1]/preceding::input[1]</value>
      <webElementGuid>7456dbe9-8dbc-4bc0-a47b-32b044de3e10</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[11]/preceding::input[1]</value>
      <webElementGuid>f2556197-baed-4fdd-9b59-7bc2f7d4d481</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[10]/div/div/div/input</value>
      <webElementGuid>9f4adaa3-5f69-48bb-89ab-2f545ceb84d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'Jumlah_BiayaPerolehan-inputEl' and @type = 'text' and @name = 'Jumlah_BiayaPerolehan']</value>
      <webElementGuid>cfa40949-a88a-4e7f-8b31-bab7400fff86</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
