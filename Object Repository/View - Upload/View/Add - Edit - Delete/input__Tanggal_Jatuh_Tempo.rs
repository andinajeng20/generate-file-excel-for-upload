<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__Tanggal_Jatuh_Tempo</name>
   <tag></tag>
   <elementGuidId>6966e568-9bf5-420c-9b53-8f76f67d0e48</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#Tanggal_Jatuh_Tempo-inputEl</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='Tanggal_Jatuh_Tempo-inputEl']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>978223e6-20a5-4e70-b2c2-073b84158587</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Tanggal_Jatuh_Tempo-inputEl</value>
      <webElementGuid>312fe35f-068a-4dee-acd5-78e4aa6d40f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>inputEl</value>
      <webElementGuid>6ef1dba5-9e44-4a23-abc4-3329efd1c4fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>f2a9f542-1142-4e0d-83d7-d86c5b8786f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>size</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>01ebebc6-0857-4bc7-aeef-c56e7e7aebdd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Tanggal_Jatuh_Tempo</value>
      <webElementGuid>01cea505-1799-47ea-8429-57794030f072</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>5fa63284-2eef-4f4e-a35f-806c2aba95a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>3ae7a64d-ee5e-41e2-a40d-891d105c4420</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>combobox</value>
      <webElementGuid>ab9af97f-0b13-4684-a369-f27fc0fa8274</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Expected Date format dd-MMM-yyyy</value>
      <webElementGuid>4e87703f-8f21-4232-8683-15dddf830c59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>b7f802db-067a-4e81-bc8f-a9ff96d095b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>1f663e38-3fd8-4b0d-944d-34da0b7702cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-owns</name>
      <type>Main</type>
      <value>Tanggal_Jatuh_Tempo-inputEl Tanggal_Jatuh_Tempo-picker-eventEl</value>
      <webElementGuid>9b7e68f1-e1c0-47a8-bdfa-2e45f5a712f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-autocomplete</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>47ad9eaa-c551-45dd-9017-99377b4c8dd1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>269ba517-5e96-43eb-90df-4e97e54b625d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-readonly</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>e00d9ebe-a1d9-4eee-a49d-6012f05d6a18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>Tanggal_Jatuh_Tempo-ariaStatusEl Tanggal_Jatuh_Tempo-ariaHelpEl</value>
      <webElementGuid>e377a1f7-ad25-41fa-8759-93f83fffa013</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>a66ad987-134d-4767-b107-ee5a7e77fbea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-form-field x-form-required-field x-form-text x-form-text-default  x-form-focus x-field-form-focus x-field-default-form-focus</value>
      <webElementGuid>73ab563a-368a-4123-ac98-1a1c8e9cd571</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>c5b32535-2dfc-442b-ba73-b44ecaa41b15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-componentid</name>
      <type>Main</type>
      <value>Tanggal_Jatuh_Tempo</value>
      <webElementGuid>0847a505-7b3a-455d-8e82-9845385ca7be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Tanggal_Jatuh_Tempo-inputEl&quot;)</value>
      <webElementGuid>68e941ce-74f0-44fa-b77c-d14c488e7f43</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/View - Upload/View/Add - Edit - Delete/iframe</value>
      <webElementGuid>5b85f5dc-4237-4dc6-8ed0-46e036eda6e6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='Tanggal_Jatuh_Tempo-inputEl']</value>
      <webElementGuid>052fc56c-edb6-46ad-ac10-f8899b0dac6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Tanggal_Jatuh_Tempo-inputWrap']/input</value>
      <webElementGuid>308a058d-3bc0-4972-b5ba-b377f4a718e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[14]/following::input[1]</value>
      <webElementGuid>00f25c2e-fe1c-43c6-9619-a25f9f9eaa88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tanggal Jatuh Tempo'])[1]/following::input[1]</value>
      <webElementGuid>54b0b4db-7245-4bbf-b326-57cef87c657c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Expected Date format dd-MMM-yyyy'])[3]/preceding::input[1]</value>
      <webElementGuid>348e250e-a5c9-43b9-91e4-22879fa7a5a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Plafon'])[1]/preceding::input[1]</value>
      <webElementGuid>ee28bc72-e478-4a40-9a9b-1b8378f14a71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[14]/div/div/div/input</value>
      <webElementGuid>fcc9f6eb-2129-45ab-9c01-73f0cf6e5390</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'Tanggal_Jatuh_Tempo-inputEl' and @type = 'text' and @name = 'Tanggal_Jatuh_Tempo' and @title = 'Expected Date format dd-MMM-yyyy']</value>
      <webElementGuid>216ef61f-368e-4332-99d4-c722641107d4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
