<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Mark As Delete No</name>
   <tag></tag>
   <elementGuidId>914b962e-89d2-4c77-9923-01e81d801cbd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder1_IsMarkAsDelete_No-inputEl</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolder1_IsMarkAsDelete_No-inputEl']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>fffc667e-047b-4127-9598-b240fb168800</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>5191d99c-372b-4595-9a60-b4563a5a18ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder1_IsMarkAsDelete_No-inputEl</value>
      <webElementGuid>996b80db-e7e3-4d1d-a2c5-d6da91f8be18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>App.IsMarkAsDelete_Group</value>
      <webElementGuid>d7d75669-d0b5-4d73-8f12-e1be292b2464</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>inputEl</value>
      <webElementGuid>074457d8-436a-43da-85e2-b840cc305193</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-form-cb-input</value>
      <webElementGuid>510b96f0-9e28-44a0-8d91-0b525dddee32</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>7f7bbc32-e99c-4534-aee2-6de8f7047cb3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>hidefocus</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>07f1d48f-b013-45d3-b25b-00197757bf3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>3d689a33-c060-4d33-abb0-fb42c47c176c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>36a8c01b-55b4-479c-b8ce-67fd1f0cb758</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>70f486a4-397d-408e-9d21-fa0acf0b8145</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>ContentPlaceHolder1_IsMarkAsDelete_No-ariaStatusEl</value>
      <webElementGuid>023f94e3-d560-4c02-8f9c-934e0c4f4c6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-componentid</name>
      <type>Main</type>
      <value>ContentPlaceHolder1_IsMarkAsDelete_No</value>
      <webElementGuid>d38f0006-3305-4107-b8de-5bb2c070cce4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder1_IsMarkAsDelete_No-inputEl&quot;)</value>
      <webElementGuid>3262b0ae-73a1-422e-859f-c8de609d0be4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/View - Upload/View/Add - Edit - Delete/iframe</value>
      <webElementGuid>702122d4-47a3-492e-a1c7-ee5d7ae126b2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolder1_IsMarkAsDelete_No-inputEl']</value>
      <webElementGuid>e40983f5-e511-409b-9833-80c37718c290</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='ContentPlaceHolder1_IsMarkAsDelete_No-displayEl']/input</value>
      <webElementGuid>aec38bf4-f89b-4700-a231-6f17253a4bda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[22]/following::input[1]</value>
      <webElementGuid>3120ec44-874f-4cd9-9554-35559a8ee270</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mark As Delete'])[1]/following::input[1]</value>
      <webElementGuid>518a5513-053e-4888-8ed0-5769463e81d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No'])[1]/preceding::input[1]</value>
      <webElementGuid>015c27c6-3b88-4b32-a8b0-8647e3186b18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Yes'])[1]/preceding::input[3]</value>
      <webElementGuid>e45c85b8-14f3-47c0-905e-bcf8bf4161de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/input</value>
      <webElementGuid>5f222b3e-2098-4758-8f6c-68e469d4881f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'radio' and @id = 'ContentPlaceHolder1_IsMarkAsDelete_No-inputEl' and @name = 'App.IsMarkAsDelete_Group']</value>
      <webElementGuid>46c08be6-1445-4fc8-829a-6771e71c2e3c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
