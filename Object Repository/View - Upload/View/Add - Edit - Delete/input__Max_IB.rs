<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__Max_IB</name>
   <tag></tag>
   <elementGuidId>e557e308-19b7-4ea4-ab1b-6cfd333321f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#Max_IB-inputEl</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='Max_IB-inputEl']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>8db0589f-70a6-41fc-a6e3-d33e451871b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Max_IB-inputEl</value>
      <webElementGuid>7f4bf0c2-60d3-4b3c-b067-ac52107edbff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>inputEl</value>
      <webElementGuid>7079d1cc-3f14-4f65-b3e4-a8193aa42c7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>83492e19-6e59-4c02-80fe-9a4817aeaba8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>size</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>7e3462e4-acce-4aa4-96ee-fdb36ec073b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Max_IB</value>
      <webElementGuid>715c04b8-c8fd-4edd-98f6-0728f4b7f18d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>9d77a959-de33-4f91-9e74-bdaf0918228d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>34cfd85d-8277-4f41-b685-f378a10493cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>spinbutton</value>
      <webElementGuid>efb9b9bb-836c-46f4-947d-f5175c4082a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>34a2d8bb-20f1-4120-84ea-6cd287dba1ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-readonly</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>8d44b359-cc69-47eb-89d7-466c718376d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>Max_IB-ariaStatusEl</value>
      <webElementGuid>aadd81d3-e206-4d18-8365-693e954dfd6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>c044b100-d10c-46e2-8144-96db2adefb38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-form-field x-form-required-field x-form-text x-form-text-default  x-form-focus x-field-form-focus x-field-default-form-focus</value>
      <webElementGuid>67b26132-8a3c-4382-9e1a-bd7386bb2737</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>3d0eec49-d0d8-43ac-9e87-252ee4b100cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-componentid</name>
      <type>Main</type>
      <value>Max_IB</value>
      <webElementGuid>bb9b63e8-b6df-491e-995f-c0407b7abb88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Max_IB-inputEl&quot;)</value>
      <webElementGuid>cbb52fb0-560d-4385-8862-b9769d6c9fc7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/View - Upload/View/Add - Edit - Delete/iframe</value>
      <webElementGuid>60d29d15-7739-48bc-a8c4-ee017e10bdd3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='Max_IB-inputEl']</value>
      <webElementGuid>ee9a3ec7-78a6-4b6d-a9e2-3afb1622ae8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Max_IB-inputWrap']/input</value>
      <webElementGuid>9008aa90-9401-478c-a9b1-d9b5a945456e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[3]/following::input[1]</value>
      <webElementGuid>a493b658-a5e7-405a-8f82-4797b12b5321</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Max IB'])[1]/following::input[1]</value>
      <webElementGuid>ccc4c80f-541b-4076-9c81-0955fcc1549c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Koifisien'])[1]/preceding::input[1]</value>
      <webElementGuid>01b0afac-60e2-4e25-a188-cda0fa57f444</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[4]/preceding::input[1]</value>
      <webElementGuid>c99f8504-0114-4ffa-9828-08d9ef75932e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/input</value>
      <webElementGuid>2e098723-ff7f-4e73-8aea-1182d68b6747</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'Max_IB-inputEl' and @type = 'text' and @name = 'Max_IB']</value>
      <webElementGuid>0e496996-5592-40bd-b558-1e4982d2fcd6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
