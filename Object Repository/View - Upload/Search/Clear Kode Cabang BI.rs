<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Clear Kode Cabang BI</name>
   <tag></tag>
   <elementGuidId>d7e5f10f-8ed9-45bd-977a-be03b541a91c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ext-element-14</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ext-element-14']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4f948def-3dd1-4971-86ef-55b0b3827264</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-clear-button</value>
      <webElementGuid>51e41186-0e5d-4510-85c3-1a6505756b6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ext-element-14</value>
      <webElementGuid>5b640fe2-61c6-4070-a7a0-f42d3e7b01b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ext-element-14&quot;)</value>
      <webElementGuid>0d7bcdd5-74f4-40db-9c15-f7a1341d28cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/View - Upload/Search/iframe</value>
      <webElementGuid>c519ba90-4b31-4312-b7c8-fcdf00760bd4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='ext-element-14']</value>
      <webElementGuid>5017ca00-c339-45eb-959b-6d884e746178</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='textfield-1020-bodyEl']/div[2]</value>
      <webElementGuid>0666f0ff-5f7f-4a77-a4bc-fdd249cddcb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cell value has been edited'])[5]/following::div[11]</value>
      <webElementGuid>cb3e349b-c57b-491f-8315-0688e1c6f2f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kode Cabang BI'])[1]/following::div[11]</value>
      <webElementGuid>c5787b66-17f4-423d-a51d-35bc3bd29b72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Flag Detail'])[1]/preceding::div[3]</value>
      <webElementGuid>6cc90a30-a4cd-4e28-9255-e32b4000b87b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cell value has been edited'])[6]/preceding::div[4]</value>
      <webElementGuid>84a73068-7f53-4ddc-b38e-bd62f5b82685</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div[2]/div/div/div/div/div/div/div[2]</value>
      <webElementGuid>362439fc-806a-4f0e-af28-7851ab162076</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'ext-element-14']</value>
      <webElementGuid>fafecbe5-87ab-4399-94c3-0745d8152044</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
