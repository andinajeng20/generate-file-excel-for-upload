<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Field 3</name>
   <tag></tag>
   <elementGuidId>4b87dabe-881f-428c-942f-cbf189301998</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder1_AdvancedFilter1_txtvaluebefore-inputEl</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ContentPlaceHolder1_AdvancedFilter1_txtvaluebefore-inputEl']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>aeaa6af2-0793-4956-9e08-6e94c84559fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder1_AdvancedFilter1_txtvaluebefore-inputEl</value>
      <webElementGuid>583138d6-c464-4603-9aea-6116247549ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>inputEl</value>
      <webElementGuid>e9b5cafe-af97-40ac-8125-e6dabc496384</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>e1760f11-1a0c-45d7-8910-7344509987df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>size</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>bef194c6-523b-4be2-8d56-ed2c5f246f3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ContentPlaceHolder1_AdvancedFilter1_txtvaluebefore</value>
      <webElementGuid>63257371-5137-4201-b6ee-13ff38119342</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>0e416489-91c7-458e-9394-6e2e13ecdd08</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>5555653f-d469-4b7d-80e9-42ee4078e98e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>textbox</value>
      <webElementGuid>9bd1ed72-687e-4034-9379-fb4347e11198</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>7746639d-4139-421e-9b3f-251805ca423b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-readonly</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>52e3fe0c-a559-4c06-901c-d2f4d17f3e80</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>ContentPlaceHolder1_AdvancedFilter1_txtvaluebefore-ariaStatusEl</value>
      <webElementGuid>2f6a73d6-b108-4d1e-bc71-f918e39564fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>e18501c0-3bfe-4363-bbeb-f58edbbe83ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-form-field x-form-text x-form-text-default  x-form-empty-field x-form-empty-field-default x-form-focus x-field-form-focus x-field-default-form-focus</value>
      <webElementGuid>50888eb5-5d39-4493-8aa5-7fe7e333234b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>697cc7f7-17a5-4dd6-bd6e-ba471a558c6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-componentid</name>
      <type>Main</type>
      <value>ContentPlaceHolder1_AdvancedFilter1_txtvaluebefore</value>
      <webElementGuid>b1b3f8c7-23c8-4b31-8e82-4f354227065f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder1_AdvancedFilter1_txtvaluebefore-inputEl&quot;)</value>
      <webElementGuid>8a9ef174-2d50-4904-9366-f0967aad7c56</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/View - Upload/Advanced Filter/iframe</value>
      <webElementGuid>d912d604-9780-45fe-aa08-3707df86dbd4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ContentPlaceHolder1_AdvancedFilter1_txtvaluebefore-inputEl']</value>
      <webElementGuid>4c79249f-cada-4e3d-8b57-79712dc774e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ContentPlaceHolder1_AdvancedFilter1_txtvaluebefore-inputWrap']/input</value>
      <webElementGuid>8e4cca7e-752a-496b-afab-69486a8f891d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Advanced Filter'])[2]/following::input[7]</value>
      <webElementGuid>e433ba88-5379-4e30-89f6-aa008681af89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Expected date format d-M-Y.'])[1]/preceding::input[3]</value>
      <webElementGuid>5cbaee8a-1b59-4add-b713-74a35e1c630e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Expected date format d-M-Y.'])[2]/preceding::input[4]</value>
      <webElementGuid>62230282-b115-4ea0-8cfe-678b5ebbe919</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/input</value>
      <webElementGuid>add46b67-e800-4d94-9f35-3680ebf81d42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'ContentPlaceHolder1_AdvancedFilter1_txtvaluebefore-inputEl' and @type = 'text' and @name = 'ContentPlaceHolder1_AdvancedFilter1_txtvaluebefore']</value>
      <webElementGuid>6087b1b2-3931-40bf-905b-e0e6cc0287ab</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
