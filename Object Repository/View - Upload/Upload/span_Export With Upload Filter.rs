<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Export With Upload Filter</name>
   <tag></tag>
   <elementGuidId>6f1de172-a4ce-4b77-bee0-3a6d4b8c730f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder1_ctl22-textEl</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='ContentPlaceHolder1_ctl22-textEl']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>20d99c1f-8d53-4ddc-a543-542cb374aacc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder1_ctl22-textEl</value>
      <webElementGuid>8a8514f6-85b2-4133-ae4c-d9fd77806b7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>textEl</value>
      <webElementGuid>1e0e4e72-a380-4035-8439-810a9c9222e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-menu-item-text x-menu-item-text-default x-menu-item-indent-no-separator</value>
      <webElementGuid>89a2b91d-db6d-4079-a62f-b49053525bba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>unselectable</name>
      <type>Main</type>
      <value>on</value>
      <webElementGuid>c30bfe12-552e-49a8-a6bd-190f6522b8a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>presentation</value>
      <webElementGuid>1cae27ae-c5a3-482a-babf-079b64d8a542</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Export With Upload Filter</value>
      <webElementGuid>b219aedf-c235-42a6-a3f9-685d364eb4b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder1_ctl22-textEl&quot;)</value>
      <webElementGuid>1db5aff9-6862-4056-a726-c463a4c8a24a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/View - Upload/Upload/iframe</value>
      <webElementGuid>d1838b01-16e3-4ec1-893c-6629708baf01</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='ContentPlaceHolder1_ctl22-textEl']</value>
      <webElementGuid>e88e5788-7148-4774-8d2b-1ac3f4ddc151</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='ContentPlaceHolder1_ctl22-itemEl']/span</value>
      <webElementGuid>dfce56c0-805a-4663-9a82-b1ca11c3d1c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Export With Filter'])[1]/following::span[1]</value>
      <webElementGuid>ac5733ac-0a83-418b-a844-15c484e5d8ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Export Without Filter'])[1]/following::span[2]</value>
      <webElementGuid>36d35134-1978-420a-91a5-e3bce0d67ea8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'ContentPlaceHolder1_ctl22-textEl', '&quot;', ')')])[1]/preceding::span[2]</value>
      <webElementGuid>6d2e96e2-c161-4ce1-a04d-b533475b84c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/a/span</value>
      <webElementGuid>809b2604-c2bc-4923-8584-449c3d8185f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'ContentPlaceHolder1_ctl22-textEl' and (text() = 'Export With Upload Filter' or . = 'Export With Upload Filter')]</value>
      <webElementGuid>1424e119-8e93-43c4-967b-1fd5feabe5d4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
