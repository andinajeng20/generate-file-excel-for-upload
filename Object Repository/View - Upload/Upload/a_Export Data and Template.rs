<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Export Data and Template</name>
   <tag></tag>
   <elementGuidId>b8fb262d-66d3-4fda-aaeb-3edec3a1e8c5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ext-element-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='ext-element-4']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>2f6cb316-2d04-4157-8a83-0f1fadd91f1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:;</value>
      <webElementGuid>50fa6374-7ef9-4d15-9945-836c052cd670</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>target</name>
      <type>Main</type>
      <value>_blank</value>
      <webElementGuid>a15f89e6-87c0-4c41-9fa6-804ca392b1e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ext-element-4</value>
      <webElementGuid>bac66ce6-54df-4e1f-ae53-2e4f75462e56</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Export Data and Template</value>
      <webElementGuid>608b6a87-d6be-4586-ba5f-1c9bf483d846</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ext-element-4&quot;)</value>
      <webElementGuid>1e452dfa-3372-45c9-ac88-1f7d6a9c7025</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/View - Upload/Upload/iframe</value>
      <webElementGuid>c4304336-318c-4d83-9628-aea3f89be89c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='ext-element-4']</value>
      <webElementGuid>1a3e6fa9-76da-4e3c-b8a6-d361cfad5fa2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='ContentPlaceHolder1_exportDataTemplate']/a</value>
      <webElementGuid>dd1e60ba-2413-40c2-86d6-fc0df0531fbb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Export Data and Template')]</value>
      <webElementGuid>0c9a4396-596c-429e-ac4d-5744d883cac5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RWA3 OP - Datasource Kerugian Operasional Upload'])[1]/following::a[1]</value>
      <webElementGuid>dd2e3e3a-e5b2-467c-811a-f6e9f6211b1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Export Template'])[1]/preceding::a[1]</value>
      <webElementGuid>97bcaa6c-5de9-4d0e-b0d9-03899434eef6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Clear Advanced Filter'])[1]/preceding::a[2]</value>
      <webElementGuid>ee411ed7-ccc3-42f2-ae69-422ac868aa8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:;')]</value>
      <webElementGuid>444173a3-467d-428d-af80-68691105eb09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a</value>
      <webElementGuid>0bfb527d-7297-43a0-b94d-51d7d0ddf8c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:;' and @id = 'ext-element-4' and (text() = 'Export Data and Template' or . = 'Export Data and Template')]</value>
      <webElementGuid>1700a439-4adb-4af4-aed9-5b9135944130</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
