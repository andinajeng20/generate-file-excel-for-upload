<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_OK</name>
   <tag></tag>
   <elementGuidId>f9e9dac8-9798-470c-82f7-0cca6c7518d5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='ContentPlaceHolder1_BtnConfirmation-btnInnerEl']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder1_BtnConfirmation-btnInnerEl</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>5e2d75c9-e839-4cb8-b23e-74a22a167c7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder1_BtnConfirmation-btnInnerEl</value>
      <webElementGuid>7523c47e-f6e8-4709-bb2a-257cf7e63587</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>btnInnerEl</value>
      <webElementGuid>e2050062-d63b-48de-94f3-9bac3c8cfc57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>unselectable</name>
      <type>Main</type>
      <value>on</value>
      <webElementGuid>33a51433-7c4e-4d86-9878-8faba8580214</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-btn-inner x-btn-inner-default-small</value>
      <webElementGuid>1d5bb086-920d-46a7-85db-2af3c8576123</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>OK</value>
      <webElementGuid>c3782c67-61d2-4617-bebc-4ee4d993b80b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder1_BtnConfirmation-btnInnerEl&quot;)</value>
      <webElementGuid>76589d7f-eb70-487f-b992-27b08310b961</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/View - Upload/Upload/iframe</value>
      <webElementGuid>e7791da0-e5ca-4e26-a5ed-fb1a3d8f92e0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='ContentPlaceHolder1_BtnConfirmation-btnInnerEl']</value>
      <webElementGuid>1434f0f3-c1ca-4b66-ad80-db1fda432f0f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='ContentPlaceHolder1_BtnConfirmation-btnEl']/span[2]</value>
      <webElementGuid>8bfc8471-ab1d-4bf9-ac78-d6d9cca9d651</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Saved into Pending Approval'])[1]/following::span[5]</value>
      <webElementGuid>97331332-b473-4254-9690-70020da2bd48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RWA3 OP - Datasource Kerugian Operasional Import'])[1]/following::span[7]</value>
      <webElementGuid>b90447c6-7a05-40ac-a04b-968d5cefe311</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Advanced Filter'])[1]/preceding::span[4]</value>
      <webElementGuid>a8e1b9a2-f961-4fae-ba22-0262c1249cec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[4]/div[2]/div[2]/div/div/a/span/span/span[2]</value>
      <webElementGuid>bc83e2df-e928-4b78-8d07-cefa48f08c5f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'ContentPlaceHolder1_BtnConfirmation-btnInnerEl' and (text() = 'OK' or . = 'OK')]</value>
      <webElementGuid>8dd07f29-8d92-4515-ac84-24bd266c4f9c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
