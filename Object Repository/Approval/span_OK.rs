<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_OK</name>
   <tag></tag>
   <elementGuidId>a9f6782b-9d0c-4a9c-b911-dacf3127eef8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='ContentPlaceHolder1_BtnConfirmation-btnInnerEl']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ContentPlaceHolder1_BtnConfirmation-btnInnerEl</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>3e654389-0da2-41c2-9dc6-54742c7205af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ContentPlaceHolder1_BtnConfirmation-btnInnerEl</value>
      <webElementGuid>89a9c200-6f65-4ed2-bfcd-07f7f36b1773</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ref</name>
      <type>Main</type>
      <value>btnInnerEl</value>
      <webElementGuid>cf2b2d7d-b07b-4bb9-923f-680e39b27829</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>unselectable</name>
      <type>Main</type>
      <value>on</value>
      <webElementGuid>fa1ad457-1d0c-40b5-a363-d0c4b85fea33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>x-btn-inner x-btn-inner-default-small</value>
      <webElementGuid>f84ddd2e-465b-4d1c-a346-1bb1eeeb5b8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>OK</value>
      <webElementGuid>1385097a-ddaa-4244-99f9-00faed475f30</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ContentPlaceHolder1_BtnConfirmation-btnInnerEl&quot;)</value>
      <webElementGuid>7a8771e6-9d5f-4a49-8e0a-79f78b30ada7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Approval/iframe</value>
      <webElementGuid>f940d2ec-803a-49f4-b22f-af4a3b8465c7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='ContentPlaceHolder1_BtnConfirmation-btnInnerEl']</value>
      <webElementGuid>ab500a04-3fd4-4617-961a-3ae7a5423796</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='ContentPlaceHolder1_BtnConfirmation-btnEl']/span[2]</value>
      <webElementGuid>0a100af9-3fb0-46eb-b1ad-3cda6fa6faee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Approved. Click Ok to Back To Module Approval.'])[1]/following::span[5]</value>
      <webElementGuid>ded7dd1e-0a4f-4ff5-b2fc-9a50c4aaada7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirmation'])[1]/following::span[7]</value>
      <webElementGuid>5c4b7c0f-cd5b-4c79-b89b-a2692fc542c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'ContentPlaceHolder1_BtnConfirmation-btnInnerEl', '&quot;', ')')])[1]/preceding::span[4]</value>
      <webElementGuid>45edbd03-cbbb-4a83-9f19-9e3337d14546</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[2]/div/div/a/span/span/span[2]</value>
      <webElementGuid>df4d5de8-f6fe-44f2-a843-d61baffe42b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'ContentPlaceHolder1_BtnConfirmation-btnInnerEl' and (text() = 'OK' or . = 'OK')]</value>
      <webElementGuid>a443fe88-d9e0-4815-9a60-9c46ba260cdb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
