import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.keyword.excel.ExcelKeywords

String excelFilePath = RunConfiguration.getProjectDir() + filePath

String sheetName = 'Sheet1'

workbook01 = ExcelKeywords.getWorkbook(excelFilePath)

sheet01 = ExcelKeywords.getExcelSheet(workbook01, sheetName)

value = ExcelKeywords.getCellValueByAddress(sheet01, cellAddress)

replaceValue = value + 1

ExcelKeywords.setValueToCellByAddress(sheet01, cellAddress, replaceValue)

ExcelKeywords.saveWorkbook(excelFilePath, workbook01)