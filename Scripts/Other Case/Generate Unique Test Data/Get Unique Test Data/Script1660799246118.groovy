import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.keyword.excel.ExcelKeywords

String excelFilePath = RunConfiguration.getProjectDir() + '/File Upload/Unique Test Data.xlsx'

String sheetName = 'Sheet1'

workbook01 = ExcelKeywords.getWorkbook(excelFilePath)

sheet01 = ExcelKeywords.getExcelSheet(workbook01, sheetName)

value = ExcelKeywords.getCellValueByAddress(sheet01, cellAddress)

return value