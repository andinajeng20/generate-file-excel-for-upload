import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

'Excel file path start from inside project dir'
String excelFilePath = (dirPath + fileName) + '.xlsx'

'Define sheet name based on excel sheet name'
String sheetName = 'Data'

'Set workbook name into variable'
workbook01 = ExcelKeywords.getWorkbook(excelFilePath)

'Set sheet name into variable'
sheet01 = ExcelKeywords.getExcelSheet(workbook01, sheetName)

'Edit cell for valid data'
Map content = new HashMap<String, ArrayList>()

content.putAt('A2', 'Insert')

content.putAt('B2', '')

ExcelKeywords.setValueToCellByAddresses(sheet01, content)

'Save excel for valid data'
excelValid = (((dirPath + fileName) + '_Valid') + '.xlsx')

ExcelKeywords.saveWorkbook(excelValid, workbook01)

'Edit cell for invalid data'
content.putAt('C2', '')

ExcelKeywords.setValueToCellByAddresses(sheet01, content)

'Save excel for invalid data'
excelInvalid = (((dirPath + fileName) + '_Invalid') + '.xlsx')

ExcelKeywords.saveWorkbook(excelInvalid, workbook01)

'Edit cell for blank data'
for(char cell = 'A'; cell <= (char) cellLimit; cell++) {
	address = Character.toString(cell) + '2'
	content.putAt(address, '')
}
ExcelKeywords.setValueToCellByAddresses(sheet01, content)

'Save excel for blank data'
excelBlank = (((dirPath + fileName) + '_Blank') + '.xlsx')

ExcelKeywords.saveWorkbook(excelBlank, workbook01)