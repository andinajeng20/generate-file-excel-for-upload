import static com.kms.katalon.core.testdata.TestDataFactory.findTestData

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.keyword.excel.ExcelKeywords

String excelFilePath = RunConfiguration.getProjectDir() + filePath

String sheetName = 'Data'

workbook01 = ExcelKeywords.getWorkbook(excelFilePath)

sheet01 = ExcelKeywords.getExcelSheet(workbook01, sheetName)

ExcelKeywords.setValueToCellByIndex(sheet01, 1, Integer.valueOf(column), customValue)

ExcelKeywords.saveWorkbook(excelFilePath, workbook01)

