import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords

'Excel file path start from inside project dir'
String excelFilePath = RunConfiguration.getProjectDir() + filePath

'Define sheet name based on excel sheet name'
String sheetName = 'Data'

'Get timestamp'
long ts = System.currentTimeMillis() / 1000

String tsAsString = ts.toString()

'Set workbook name into variable'
workbook01 = ExcelKeywords.getWorkbook(excelFilePath)

'Set sheet name into variable'
sheet01 = ExcelKeywords.getExcelSheet(workbook01, sheetName)

'Edit excel value in specific cell'
ExcelKeywords.setValueToCellByIndex(sheet01, 1, Integer.valueOf(column), tsAsString)

'Save edited value'
ExcelKeywords.saveWorkbook(excelFilePath, workbook01)

