import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.exception.StepErrorException as StepErrorException

WebDriver driver = DriverFactory.getWebDriver()

WebUI.click(findTestObject('Dashboard - Home/Menu/Clear Filter'))

MenuPath += MenuSelection

String[] pathMenu = MenuPath.split('/')

String tempXPath = ''

for (int i = 0; i < pathMenu.length; i++) {
    if (tempXPath.trim().isEmpty()) {
        tempXPath = (('//span[@class="x-tree-node-text " and text()="' + (pathMenu[i])) + '"]')
    } else {
        tempXPath += (('/../../../../../following-sibling::*//span[@class="x-tree-node-text " and text()="' + (pathMenu[
        i])) + '"]')
    }
    
    TestObject nodeMenu = new TestObject()

    if (i < (pathMenu.length - 1)) {
        nodeMenu = WebUI.modifyObjectProperty(nodeMenu, 'xpath', 'equals', ((tempXPath + '/../div[') + (i + 1)) + ']', true)
    } else {
        nodeMenu = WebUI.modifyObjectProperty(nodeMenu, 'xpath', 'equals', tempXPath, true)
    }
    
    WebUI.click(nodeMenu)
}

WebUI.delay(2)

com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords.takeScreenshot()

