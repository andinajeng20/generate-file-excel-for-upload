import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Dashboard - Home/span_Setting Branch  Period'))

WebUI.waitForElementClickable(findTestObject('Dashboard - Home/Setting Branch Period/span_Save'), 0)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Dashboard - Home/Setting Branch Period/Clear Kantor Cabang'))

WebUI.click(findTestObject('Dashboard - Home/Setting Branch Period/div_Dropdown Branch'))

WebUI.setText(findTestObject('Dashboard - Home/Setting Branch Period/input_Kantor Cabang'), GlobalVariable.branch)

WebUI.sendKeys(findTestObject('Dashboard - Home/Setting Branch Period/input_Kantor Cabang'), Keys.chord(Keys.ENTER))

try {
    WebUI.click(findTestObject('Dashboard - Home/Setting Branch Period/Clear Internal Branch'), FailureHandling.STOP_ON_FAILURE)

    WebUI.click(findTestObject('Dashboard - Home/Setting Branch Period/div__Dropdown Internal Branch'), FailureHandling.STOP_ON_FAILURE)

    WebUI.setText(findTestObject('Dashboard - Home/Setting Branch Period/input_Internal Branch'), GlobalVariable.internalBranch, 
        FailureHandling.STOP_ON_FAILURE)

    WebUI.sendKeys(findTestObject('Dashboard - Home/Setting Branch Period/input_Report Period'), Keys.chord(Keys.ENTER), 
        FailureHandling.STOP_ON_FAILURE)
}
catch (Exception ex) {
    WebUI.delay(3)
} 

WebUI.clearText(findTestObject('Dashboard - Home/Setting Branch Period/input_Report Period'))

WebUI.setText(findTestObject('Dashboard - Home/Setting Branch Period/input_Report Period'), GlobalVariable.period)

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Dashboard - Home/Setting Branch Period/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

