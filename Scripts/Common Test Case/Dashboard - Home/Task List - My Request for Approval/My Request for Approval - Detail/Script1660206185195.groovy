import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.setText(findTestObject('Dashboard - Home/My Request for Approval/input_Model Name'), keyword)

WebUI.delay(5)

WebUI.waitForElementClickable(findTestObject('Dashboard - Home/My Request for Approval/span_Detail First Child Element'), 
    0)

WebUI.click(findTestObject('Dashboard - Home/My Request for Approval/span_Detail First Child Element'))

WebUI.callTestCase(findTestCase('Common Test Case/Search by Column/Search Column'), [('keyword') : action, ('column') : 'Action'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Approval/span_Detail First Child Element'))

WebUI.waitForElementClickable(findTestObject('Approval/span_Back'), 0)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Approval/span_Back'))

WebUI.takeScreenshot()

