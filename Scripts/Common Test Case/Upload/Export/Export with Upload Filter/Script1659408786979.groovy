import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementClickable(findTestObject('View - Upload/Upload/a_Export Data and Template'), 0)

WebUI.click(findTestObject('View - Upload/Upload/a_Export Data and Template'))

WebUI.waitForElementClickable(findTestObject('View - Upload/Upload/span_Export With Upload Filter'), 0)

WebUI.click(findTestObject('View - Upload/Upload/span_Export With Upload Filter'))

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.waitForElementClickable(findTestObject('View - Upload/Upload/Upload Filter/span_Export Filtered'), 0)

WebUI.click(findTestObject('View - Upload/Upload/Upload Filter/span_Export Filtered'))

WebUI.delay(5)

WebUI.waitForElementClickable(findTestObject('View - Upload/Upload/span_OK Error Popup'), 0)

WebUI.takeScreenshot()

WebUI.enhancedClick(findTestObject('View - Upload/Upload/span_OK Error Popup'))

WebUI.click(findTestObject('View - Upload/Upload/Upload Filter/a_Export Template'))

WebUI.delay(3)

WebUI.takeFullPageScreenshot()

String dirName = RunConfiguration.getProjectDir()

String fullPath = dirName + path

WebUI.uploadFile(findTestObject('View - Upload/Upload/Upload Filter/input_Browse File For Filter'), fullPath)

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.waitForElementClickable(findTestObject('View - Upload/Upload/Upload Filter/div_Add Selected Fields'), 0)

WebUI.click(findTestObject('View - Upload/Upload/Upload Filter/div_Add Selected Fields'))

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/Upload/Upload Filter/input_Search Selected Fields'), searchKeywords)

WebUI.delay(3)

WebUI.waitForElementClickable(findTestObject('View - Upload/Upload/Upload Filter/span_Checkbox Selected Fields'), 0)

WebUI.click(findTestObject('View - Upload/Upload/Upload Filter/span_Checkbox Selected Fields'))

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/Upload/Upload Filter/span_Export Filtered'))

WebUI.delay(5)

WebUI.takeFullPageScreenshot(FailureHandling.STOP_ON_FAILURE)

