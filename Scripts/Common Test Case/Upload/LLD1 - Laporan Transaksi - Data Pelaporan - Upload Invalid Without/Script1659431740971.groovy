import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Upload/a_Export Template'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.uploadFile(findTestObject('Upload/input_Browse_ContentPlaceHolder1_FileUploadField1'), findTestData('Directory Upload All').getValue(
        3, 4))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View/span_Upload Without Validate'))

WebUI.delay(10)

WebUI.scrollToElement(findTestObject('Objek Upload/span_Export Invalid Data'), 15)

WebUI.click(findTestObject('Tombol Plus Upload/Plus 1'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Tombol Plus Upload/Plus 1'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.delay(2)

WebUI.click(findTestObject('View/span_Save_Upload'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Objek Upload/OK Popup'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Objek Upload/span_Export Invalid Data'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Objek Upload/span_Back'))

WebUI.delay(5)

WebUI.takeScreenshot()

