import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords

WebUI.waitForElementPresent(findTestObject('Upload/a_Export Template'), 10)

WebUI.click(findTestObject('Upload/a_Export Template'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.waitForElementPresent(findTestObject('Upload/input_Browse_ContentPlaceHolder1_FileUploadField1'), 10)

WebUI.uploadFile(findTestObject('Upload/input_Browse_ContentPlaceHolder1_FileUploadField1'), findTestData('Directory Upload All').getValue(
        3, 5))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.waitForElementPresent(findTestObject('Upload/span_Upload With Validate'), 10)

WebUI.click(findTestObject('Upload/span_Upload With Validate'))

WebUI.delay(15)

WebUI.takeScreenshot()

WebUI.waitForElementPresent(findTestObject('Objek Upload/span_Back'), 0)

WebUI.click(findTestObject('Objek Upload/span_Back'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.delay(5)

WebUI.waitForElementPresent(findTestObject('Upload/input_Browse_ContentPlaceHolder1_FileUploadField1'), 10)

WebUI.uploadFile(findTestObject('Upload/input_Browse_ContentPlaceHolder1_FileUploadField1'), findTestData('Directory Upload All').getValue(
        3, 5))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.waitForElementPresent(findTestObject('Upload/span_Upload With Validate'), 10)

WebUI.click(findTestObject('Upload/span_Upload With Validate'))

WebUI.delay(15)

WebUI.takeScreenshot()

WebUI.waitForElementPresent(findTestObject('Upload/Save Upload'), 0)

WebUI.click(findTestObject('Upload/Save Upload'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.waitForElementPresent(findTestObject('Upload/OK Upload'), 10)

WebUI.click(findTestObject('Upload/OK Upload'))

WebUI.delay(5)

WebUI.takeScreenshot()

String excelFilePath = RunConfiguration.getProjectDir() + '/File Upload/LLD1 Data Pelaporan/LLD1-LaporanTransaksi_ValidWith.xlsx'

String sheetName = 'Data'

long ts = System.currentTimeMillis() / 1000

String tsAsString = ts.toString()

workbook01 = ExcelKeywords.getWorkbook(excelFilePath)

sheet01 = ExcelKeywords.getExcelSheet(workbook01, sheetName)

ExcelKeywords.setValueToCellByIndex(sheet01, 1, 8, tsAsString)

ExcelKeywords.saveWorkbook(excelFilePath, workbook01)

