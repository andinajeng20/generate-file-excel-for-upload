import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

WebUI.setText(findTestObject('Login - Logout/input_User ID'), '')

WebUI.setEncryptedText(findTestObject('Login - Logout/input_Password'), GlobalVariable.password)

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Login - Logout/button_Sign In'))

WebUI.delay(3)

WebUI.takeScreenshot()

errValidationMsg = WebUI.getText(findTestObject('Login - Logout/span_User ID is required'))

CustomKeywords.'kms.turing.katalon.plugins.assertj.StringAssert.equals'(errValidationMsg, 'User ID is required', true, 'User ID Validation Error Message', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Login - Logout/input_User ID'), GlobalVariable.userMaker)

WebUI.setEncryptedText(findTestObject('Login - Logout/input_Password'), '')

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Login - Logout/button_Sign In'))

WebUI.delay(3)

WebUI.takeScreenshot()

errValidationMsg = WebUI.getText(findTestObject('Login - Logout/span_Password is required'))

CustomKeywords.'kms.turing.katalon.plugins.assertj.StringAssert.equals'(errValidationMsg, 'Password is required', true, 
    'Password Validation Error Message', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Login - Logout/input_User ID'), '')

WebUI.setEncryptedText(findTestObject('Login - Logout/input_Password'), '')

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Login - Logout/button_Sign In'))

WebUI.delay(3)

WebUI.takeScreenshot()

errValidationMsg = WebUI.getText(findTestObject('Login - Logout/span_User ID is required'))

CustomKeywords.'kms.turing.katalon.plugins.assertj.StringAssert.equals'(errValidationMsg, 'User ID is required', true, 'User ID Validation Error Message', 
    FailureHandling.CONTINUE_ON_FAILURE)

errValidationMsg = WebUI.getText(findTestObject('Login - Logout/span_Password is required'))

CustomKeywords.'kms.turing.katalon.plugins.assertj.StringAssert.equals'(errValidationMsg, 'Password is required', true, 
    'Password Validation Error Message', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Login - Logout/input_User ID'), GlobalVariable.userMaker)

WebUI.setEncryptedText(findTestObject('Login - Logout/input_Password'), 'dsES2STtqWo=')

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Login - Logout/button_Sign In'))

WebUI.delay(3)

WebUI.takeScreenshot()

errValidationMsg = WebUI.getText(findTestObject('Login - Logout/span_Invalid Credential.Please Contact Your Administrator'))

CustomKeywords.'kms.turing.katalon.plugins.assertj.StringAssert.equals'(errValidationMsg, 'Invalid Credential.Please Contact Your Administrator', 
    true, 'Invalid Credential Error Message', FailureHandling.CONTINUE_ON_FAILURE)

