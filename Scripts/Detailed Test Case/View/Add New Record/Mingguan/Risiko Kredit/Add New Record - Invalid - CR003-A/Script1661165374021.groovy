import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/View/Mingguan/Pick Menu - CR003-A - View'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('View - Upload/View/a_Add New Record'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Report_Date'), '08-2022')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Kode_Cabang_BI'), '000000 - BANK HSBC IND KP')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Flag_Detail'), 'D')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/textarea__Nama_Debitur'), 'TEST')

uniqueValue = WebUI.callTestCase(findTestCase('Other Case/Generate Unique Test Data/Get Unique Test Data'), [('filePath') : '/File Upload/Unique Test Data.xlsx'
        , ('cellAddress') : 'B1'], FailureHandling.STOP_ON_FAILURE)

uniqueValueString = Integer.toString(uniqueValue)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nomor_CIF'), uniqueValueString)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/textarea__Group_Usaha'), 'TEST')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__FK_Jenis_Debitur'), 'I - Individual')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nomor_Identitas'), '1234567890')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Baki_Debet_Sebelum'), '1000')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Baki_Debet_Sesudah'), '2000')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Selisih_Baki_Debet'), '1000')

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/input_Mark As Delete Yes'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__AccessCode'), 'TEST')

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/input_No_App.Include_In_Report_Group'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Report_Date'), '08-Aug-2022')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Flag_Detail'), 'A')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Flag_Detail'), 'D')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nomor_CIF'), '@#$%')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nomor_CIF'), uniqueValueString)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/textarea__Group_Usaha'), '@#$%')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/textarea__Group_Usaha'), 'TEST')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nomor_Identitas'), '@bcd')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nomor_Identitas'), '1234567890')

WebUI.delay(5)

WebUI.takeScreenshot()

