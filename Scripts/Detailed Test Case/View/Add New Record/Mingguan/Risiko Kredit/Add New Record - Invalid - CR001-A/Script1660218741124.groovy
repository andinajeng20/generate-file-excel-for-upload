import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/View/Mingguan/Pick Menu - CR001-A - View'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('View - Upload/View/a_Add New Record'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Report_Date'), '08-2022')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Kode_Cabang_BI'), '000000 - BANK HSBC IND KP')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Flag_Detail'), 'D')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/textarea__Nama_Debitur'), 'TEST')

uniqueValue = WebUI.callTestCase(findTestCase('Other Case/Generate Unique Test Data/Get Unique Test Data'), [('filePath') : '/File Upload/Unique Test Data.xlsx'
        , ('cellAddress') : 'B1'], FailureHandling.STOP_ON_FAILURE)

uniqueValueString = Integer.toString(uniqueValue + 1)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nomor_CIF'), uniqueValueString)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nomor_Rekening'), uniqueValueString)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/textarea__Group_Usaha'), 'TEST')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__FK_Jenis_Debitur'), 'I - Individual')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nomor_Identitas'), '1234567890')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__FK_Sektor_Ekonomi'), '000001 - Kegiatan yang Belum Jelas Batasannya Perorangan')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__FK_Jenis_Penggunaan'), '1 - Modal Kerja')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Tanggal_Mulai'), '08-Aug-2022')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Tanggal_Jatuh_Tempo'), '08-Sep-2022')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Plafon'), '666')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Baki_Debet'), '666')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nominal_Suku_Bunga'), '1')

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/div_Jenis Agunan Dropdown'))

WebUI.delay(5)

WebUI.waitForElementClickable(findTestObject('View - Upload/View/Add - Edit - Delete/span_Jenis Agunan Checkbox Second Child'), 
    30)

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Jenis Agunan Checkbox Second Child'))

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/div_Close Jenis Agunan'))

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nilai_Agunan'), '6000')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__FK_Segmen'), '03 - Mikro')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/textarea__Nama_Bank_Lain_Sebagai_Kreditur'), 'TEST')

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/input_Mark As Delete Yes'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__AccessCode'), 'TEST')

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/input_No_App.Include_In_Report_Group'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Report_Date'), '08-Aug-2022')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Flag_Detail'), 'A')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Flag_Detail'), 'D')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nomor_CIF'), '@#$%')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nomor_CIF'), uniqueValueString)

invalidNoRekening = Integer.toString(uniqueValue)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nomor_Rekening'), invalidNoRekening)

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nomor_Rekening'), '@bcd')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nomor_Rekening'), uniqueValueString)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/textarea__Group_Usaha'), '@#$%')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/textarea__Group_Usaha'), 'TEST-TEST')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/textarea__Group_Usaha'), 'TEST')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nomor_Identitas'), '@bcd')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nomor_Identitas'), '1234567890')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Tanggal_Mulai'), '08-Sep-2022')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Tanggal_Jatuh_Tempo'), '08-Aug-2022')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Tanggal_Mulai'), '08-Aug-2022')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Tanggal_Jatuh_Tempo'), '08-Sep-2022')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nominal_Suku_Bunga'), '121')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nominal_Suku_Bunga'), '1')

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/div_Jenis Agunan Dropdown'))

WebUI.delay(5)

WebUI.waitForElementClickable(findTestObject('View - Upload/View/Add - Edit - Delete/span_Jenis Agunan Checkbox First Child'), 
    30)

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Jenis Agunan Checkbox Second Child'))

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Jenis Agunan Checkbox First Child'))

WebUI.sendKeys(findTestObject('View - Upload/View/Add - Edit - Delete/span_Jenis Agunan Checkbox First Child'), Keys.chord(
        Keys.ESCAPE))

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nilai_Agunan'), '6000')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/div_Jenis Agunan Dropdown'))

WebUI.delay(5)

WebUI.waitForElementClickable(findTestObject('View - Upload/View/Add - Edit - Delete/span_Jenis Agunan Checkbox Second Child'), 
    30)

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Jenis Agunan Checkbox First Child'))

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Jenis Agunan Checkbox Second Child'))

WebUI.sendKeys(findTestObject('View - Upload/View/Add - Edit - Delete/span_Jenis Agunan Checkbox Second Child'), Keys.chord(
        Keys.ESCAPE))

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nilai_Agunan'), '0')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

