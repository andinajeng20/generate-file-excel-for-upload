import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('View - Upload/View/a_Add New Record'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Report_Date'), '08-Aug-2022')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Kode_Cabang_BI'), '000000 - BANK HSBC IND KP')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Flag_Detail'), 'D')

timestamp = CustomKeywords.'nds.NDSCustomKeyword.getTimestamp'()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nomor_CIF'), timestamp)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/textarea__Nama_Nasabah'), 'TEST')

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('View - Upload/View/Add - Edit - Delete/input__NominalTabungan_Rupiah'), 0)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__NominalTabungan_Rupiah'), '1000000')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__NominalTabungan_Valas'), '1000')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__NominalGiro_Rupiah'), '1000000')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__NominalGiro_Valas'), '1000')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__NominalDeposito_Rupiah'), '1000000')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__NominalDeposito_Valas'), '1000')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Total'), '10000000')

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/input_Mark As Delete Yes'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/input_No_App.Include_In_Report_Group'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__AccessCode'), 'TEST')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.waitForElementPresent(findTestObject('View - Upload/View/Add - Edit - Delete/span_Data Saved into Pending Approval'), 
    0)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_OK'))

WebUI.delay(5)

WebUI.takeScreenshot()

