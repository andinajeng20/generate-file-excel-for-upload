import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/View/Harian/Pick Menu - MK002-A - View'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('View - Upload/View/a_Add New Record'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Report_Date'), '08-2022')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Kode_Cabang_BI'), '000000 - BANK HSBC IND KP')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Flag_Detail'), 'D')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/textarea__Nama_Pembeli'), 'TEST')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Status_Pembeli'), '110 - Counter-Party merupakan Bank di dalam negeri')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Identitas_CounterParty'), '000000000100199 - ASTRA LAINNYA')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Negara_Pembeli'), 'ID - Indonesia')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Jumlah_Transaksi'), '1')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Volume_Rp'), '10000000')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Volume_Valas'), '2000')

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/input_Mark As Delete Yes'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__AccessCode'), 'TEST')

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/input_No_App.Include_In_Report_Group'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Report_Date'), '08-Aug-2022')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Flag_Detail'), 'A')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.delay(5)

WebUI.takeScreenshot()

