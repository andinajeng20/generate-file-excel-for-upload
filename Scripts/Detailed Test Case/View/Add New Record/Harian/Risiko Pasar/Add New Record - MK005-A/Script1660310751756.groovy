import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('View - Upload/View/a_Add New Record'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Report_Date'), '08-Aug-2022')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Kode_Cabang_BI'), '000000 - BANK HSBC IND KP')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Flag_Detail'), 'D')

WebUI.callTestCase(findTestCase('Other Case/Generate Unique Test Data/Generate Unique Test Data'), [('filePath') : '/File Upload/Unique Test Data.xlsx'
        , ('cellAddress') : 'B6'], FailureHandling.STOP_ON_FAILURE)

uniqueValue = WebUI.callTestCase(findTestCase('Other Case/Generate Unique Test Data/Get Unique Test Data'), [('filePath') : '/File Upload/Unique Test Data.xlsx'
        , ('cellAddress') : 'B6'], FailureHandling.STOP_ON_FAILURE)

uniqueValueString = Integer.toString(uniqueValue)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Kode_ISIN'), uniqueValueString)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/textarea__Nama_SSB'), 'TEST')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Jenis_SSB'), '042 - Sertifikat Bank Indonesia')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Kategori_Pengukuran'), '1 - Hold To Maturity / HTM')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Nominal Nilai Par'), '1,000')

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('View - Upload/View/Add - Edit - Delete/input__Jumlah_BiayaPerolehan'), 0)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Jumlah_BiayaPerolehan'), '1000')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Golongan_Pihak_Lawan'), '002 - PT. BANK RAKYAT INDONESIA (PERSERO), Tbk')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Golongan_Penerbit'), '008 - PT. BANK MANDIRI (PERSERO), Tbk')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Peringkat_Penerbit_SSB'), '3')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Lembaga_Pemeringkat'), '00 - NA')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Mata_Uang'), 'IDR - Indonesian Rupiah')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__SukuBunga_Diskonto'), '1')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Durasi_Bulan'), '1')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Tanggal_Jatuh_Tempo'), '12-Sep-2022')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Jenis_Bunga'), '1 - Suku Bunga Fixed')

WebUI.sendKeys(findTestObject('View - Upload/View/Add - Edit - Delete/input__Jenis_Bunga'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/input_Mark As Delete Yes'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__AccessCode'), 'TEST')

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/input_No_App.Include_In_Report_Group'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.waitForElementPresent(findTestObject('View - Upload/View/Add - Edit - Delete/span_Data Saved into Pending Approval'), 
    0)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_OK'))

WebUI.delay(5)

WebUI.takeScreenshot()

