import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('View - Upload/View/a_Add New Record'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Report_Date'), '08-Aug-2022')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Kode_Cabang_BI'), '000000 - BANK HSBC IND KP')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Flag_Detail'), 'D')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/textarea__Nama_Pembeli'), 'TEST')

WebUI.takeScreenshot()

WebUI.scrollToElement(findTestObject('View - Upload/View/Add - Edit - Delete/input__Status_Pembeli'), 0)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Status_Pembeli'), '110 - Counter-Party merupakan Bank di dalam negeri')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Identitas_CounterParty'), '087 - PT. BANK HSBC INDONESIA')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Negara_Pembeli'), 'ID - Indonesia')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Jumlah_Transaksi'), '1')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Volume_Rp'), '10000000')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Volume_Valas'), '2000')

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/input_Mark As Delete Yes'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__AccessCode'), 'TEST')

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/input_No_App.Include_In_Report_Group'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.waitForElementPresent(findTestObject('View - Upload/View/Add - Edit - Delete/span_Data Saved into Pending Approval'), 
    0)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_OK'))

WebUI.delay(5)

WebUI.takeScreenshot()

