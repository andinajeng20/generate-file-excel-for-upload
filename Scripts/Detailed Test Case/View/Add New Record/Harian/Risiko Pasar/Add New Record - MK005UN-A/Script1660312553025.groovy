import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('View - Upload/View/a_Add New Record'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Report_Date'), '08-Aug-2022')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Kode_Cabang_BI'), '000000 - BANK HSBC IND KP')

uniqueValue = WebUI.callTestCase(findTestCase('Other Case/Generate Unique Test Data/Get Unique Test Data'), [('filePath') : '/File Upload/Unique Test Data.xlsx'
        , ('cellAddress') : 'B6'], FailureHandling.STOP_ON_FAILURE)

uniqueValueString = Integer.toString(uniqueValue)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Kode_ISIN'), uniqueValueString)

String dirName = RunConfiguration.getProjectDir()

String fullPath = dirName + path

WebUI.uploadFile(findTestObject('View - Upload/View/Add - Edit - Delete/input_Browse_File_Pendukung'), fullPath)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__MarkAsDelete'), 'No')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__AccessCode'), 'TEST')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.waitForElementPresent(findTestObject('View - Upload/View/Add - Edit - Delete/span_Data Saved into Pending Approval'), 
    0)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_OK'))

WebUI.delay(5)

WebUI.takeScreenshot()

