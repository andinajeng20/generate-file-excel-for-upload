import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(findTestObject('View - Upload/View/a_Add New Record'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Report_Date'), '08-Aug-2022')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Kode_Cabang_BI'), '000000 - BANK HSBC IND KP')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__Identitas_CounterParty'), '000000000100199 - ASTRA LAINNYA')

String dirName = RunConfiguration.getProjectDir()

String fullPath = dirName + path

WebUI.uploadFile(findTestObject('View - Upload/View/Add - Edit - Delete/input_Browse_File_Pendukung'), fullPath)

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__MarkAsDelete'), 'No')

WebUI.setText(findTestObject('View - Upload/View/Add - Edit - Delete/input__AccessCode'), 'TEST')

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_Save'))

WebUI.waitForElementPresent(findTestObject('View - Upload/View/Add - Edit - Delete/span_Data Saved into Pending Approval'), 
    0)

WebUI.takeScreenshot()

WebUI.click(findTestObject('View - Upload/View/Add - Edit - Delete/span_OK'))

WebUI.delay(5)

WebUI.takeScreenshot()

