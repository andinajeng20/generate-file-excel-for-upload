import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Login Maker'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/View/Mingguan/Pick Menu - CR005-A - View'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/View/Add New Record/Mingguan/Risiko Kredit/Add New Record - CR005-A'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Login Checker'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/Approval/Mingguan/Pick Menu - CR005-A - Approval'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Approval/Checker Approve'), [('action') : 'Insert'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Login Maker'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/View/Data Detail/Pick Menu - CR005UN-A - View'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/View/Cancel Add'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/View/Add New Record/Data Detail/Add New Record - CR005UN-A'), [('path') : findTestData(
            'Directory File Pendukung').getValue(1, 1)], FailureHandling.STOP_ON_FAILURE)

WebUI.refresh()

WebUI.callTestCase(findTestCase('Common Test Case/Dashboard - Home/Task List - My Request for Approval/My Request for Approval - Detail'), 
    [('keyword') : 'Data Detail 50 Debitur Upgrade Kualitas Kredit Berdasarkan Baki Debet Terbesar', ('action') : 'Insert'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Login Checker'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Dashboard - Home/Task List - My Request for Approval/Task List - Detail'), 
    [('keyword') : 'Data Detail 50 Debitur Upgrade Kualitas Kredit Berdasarkan Baki Debet Terbesar', ('action') : 'Insert'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/Approval/Data Detail/Pick Menu - CR005UN-A - Approval'), [:], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Approval/Checker Reject'), [('action') : 'Insert'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/View/Data Detail/Pick Menu - CR005UN-A - View'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Login Maker'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/View/Data Detail/Pick Menu - CR005UN-A - View'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/View/Add New Record/Data Detail/Add New Record - CR005UN-A'), [('path') : findTestData(
            'Directory File Pendukung').getValue(1, 1)], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Login Checker'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/Approval/Data Detail/Pick Menu - CR005UN-A - Approval'), [:], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Approval/Checker Approve'), [('action') : 'Insert'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/View/Data Detail/Pick Menu - CR005UN-A - View'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Login Maker'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/View/Data Detail/Pick Menu - CR005UN-A - View'), [:], FailureHandling.STOP_ON_FAILURE)

