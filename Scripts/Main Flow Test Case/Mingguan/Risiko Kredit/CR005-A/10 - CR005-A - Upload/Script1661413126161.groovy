import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/Upload/Mingguan/Pick Menu - CR005-A - Upload'), [:], FailureHandling.STOP_ON_FAILURE)

'Upload Blank File'
not_run: WebUI.callTestCase(findTestCase('Common Test Case/Upload/Upload Invalid Error'), [('path') : findTestData('Directory Upload File').getValue(
            1, 21), ('uploadType') : 'Upload With Validate'], FailureHandling.STOP_ON_FAILURE)

'Upload Invalid Format (CSV)'
WebUI.callTestCase(findTestCase('Common Test Case/Upload/Upload Invalid Error'), [('path') : findTestData('Directory Upload File').getValue(
            1, 22), ('uploadType') : 'Upload With Validate'], FailureHandling.STOP_ON_FAILURE)

'Upload Invalid File'
WebUI.callTestCase(findTestCase('Common Test Case/Upload/Upload Invalid Error'), [('path') : findTestData('Directory Upload File').getValue(
            1, 23), ('uploadType') : 'Upload Without Validate'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Edit Excel Cell Value with Timestamp'), [('filePath') : findTestData('Directory Upload File').getValue(
            1, 25), ('column') : '6'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Edit Excel Cell Value with Timestamp'), [('filePath') : findTestData('Directory Upload File').getValue(
            1, 25), ('column') : '7'], FailureHandling.STOP_ON_FAILURE)

'Upload Valid With Validate'
WebUI.callTestCase(findTestCase('Common Test Case/Upload/Upload Valid'), [('path') : findTestData('Directory Upload File').getValue(
            1, 25), ('uploadType') : 'Upload With Validate'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Login Checker'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/Approval/Mingguan/Pick Menu - CR005-A - Approval'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Approval/Checker Approve'), [('action') : 'Import'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/View/Mingguan/Pick Menu - CR005-A - View'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Login Maker'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/View/Mingguan/Pick Menu - CR005-A - View'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/Upload/Mingguan/Pick Menu - CR005-A - Upload'), [:], FailureHandling.STOP_ON_FAILURE)

'Upload Invalid Without Validate'
WebUI.callTestCase(findTestCase('Common Test Case/Upload/Upload Invalid Content'), [('path') : findTestData('Directory Upload File').getValue(
            1, 24), ('uploadType') : 'Upload Without Validate'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/Upload/Mingguan/Pick Menu - CR005-A - Upload'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Edit Excel Cell Value with Timestamp'), [('filePath') : findTestData('Directory Upload File').getValue(
            1, 25), ('column') : '6'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Edit Excel Cell Value with Timestamp'), [('filePath') : findTestData('Directory Upload File').getValue(
            1, 25), ('column') : '7'], FailureHandling.STOP_ON_FAILURE)

'Upload Valid Without Validate'
WebUI.callTestCase(findTestCase('Common Test Case/Upload/Upload Valid'), [('path') : findTestData('Directory Upload File').getValue(
            1, 25), ('uploadType') : 'Upload Without Validate'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Login Checker'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/Approval/Mingguan/Pick Menu - CR005-A - Approval'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Approval/Checker Reject'), [('action') : 'Import'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/View/Mingguan/Pick Menu - CR005-A - View'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Login Maker'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Detailed Test Case/Pick Menu/View/Mingguan/Pick Menu - CR005-A - View'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Login - Logout/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

